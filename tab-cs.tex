\subsection{Content Store (CS)}\label{sec:tab-cs}

The Content Store (CS) is a cache of Data packets.
Forwarding pipelines (Section~\ref{sec:fw}) places arriving Data packets in the CS, so that future Interests requesting the same Data can be satisfied without forwarding further.

The CS offers procedures to insert a Data packet, find cached Data that matches an Interest, and enumeration of cached Data.
Section~\ref{sec:tab-cs-api} describes the semantics of those procedures and their usage.

The CS is implemented in (\texttt{nfd::cs::Cs}) class.
The implementation consists of two parts: a lookup table, and a cache replacement policy.
The lookup table (Section~\ref{sec:tab-cs-table}) is a name-based index of CS entries, in which cached Data packets are stored.
The cache replacement policy (Section~\ref{sec:tab-cs-policy}) is responsible for keeping the CS under capacity limits.
It separately maintains a cleanup index in order to determine which entry to evict when the CS is full.
NFD offers multiple cache replacement policies including a priority FIFO policy and a LRU policy, which can be selected in NFD configuration file.

\subsubsection{Semantics and Usage}\label{sec:tab-cs-api}

\paragraph{Insertion}

Data packets are inserted to the CS (\texttt{Cs::insert}) either in the \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) or in the \textit{Data unsolicited pipeline} (Section~\ref{sec:fw-data-unsolicited}), after forwarding ensures eligibility of the Data packet to be processed at all (e.g., that the Data packet does not violate the name-based scope control~\cite{ScopeControl}).

Before storing the Data packet, the \textbf{admission policy} is evaluated.
Local applications can give hints to the admission policy via NDNLPv2~\cite{NDNLPv2} \textit{CachePolicy} field attached to the Data packet.
These hints are considered advisory.

After passing the admission policy, the Data packet is stored, along with the time point at which it would become stale and can no longer satisfy an Interest with MustBeFresh Selector.

The CS is configured with a capacity limit, which is checked at this point.
If the insertion of this new Data packet causes the CS to exceed the capacity limit, the \textbf{cache replacement policy} evicts excessive entries to bring the CS under capacity limit.

\paragraph{Lookup}

The CS offers an asynchronous lookup API (\texttt{Cs::find}).
\textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) invokes this API with an incoming Interest.
The search algorithm gives the Data packet that best matches the Interest to \textit{ContentStore hit pipeline} (Section~\ref{sec:fw-cs-hit}), or informs \textit{ContentStore miss pipeline} (Section~\ref{sec:fw-cs-miss}) if there's no match.

\paragraph{Enumeration and Entry Abstraction}

The Content Store can be enumerated via forward iterators.
This feature is not directly used in NFD, but it could be useful in simulation environments.

In order to keep a stable enumeration interface, but still allow the CS implementation to be replaced, the iterator is dereferenced to \texttt{nfd::cs::Entry} type, which is an abstraction of a CS entry.
The public API of this type allows the caller to get the Data packet, whether it's unsolicited, and the time point at which it would become stale.
A Content Store implementation may define its own concrete type, and convert to the abstraction type during enumeration.


\subsubsection{Lookup Table}\label{sec:tab-cs-table}

The \emph{Table} is an ordered container that stores concrete entries (\texttt{nfd::cs::EntryImpl}, subclass of the entry abstraction type).
This container is sorted by Data Name with implicit digest.%
\footnote{Implicit digest computation is CPU-intensive. The Table has an optimization that avoids implicit digest computation in most cases while still guarantee correct sorting order.}

Lookups are performed entirely using the Table.
The lookup procedure (\texttt{Cs::find*}) is optimized to minimize the number of entries visited in the expected case.
In the worst case, a lookup would visit all entries that has Interest Name as a prefix.

Although the lookup API is asynchronous, the current implementation does lookups synchronously.

The \emph{Table} uses \texttt{std::set} as the underlying container because its good performance in benchmarks.
A previous CS implementation used a skip list, but its performance was worse than \texttt{std::set}, probably due to algorithm complexity and code quality.


\subsubsection{Cache Replacement Policy}\label{sec:tab-cs-policy}

The cache replacement policy keeps the CS under its capacity limit.
The main capacity limit is measured in terms of number of cached Data packets.
This metric is chosen over Data packet size because the memory overhead of table index can be significant for small packets so that a packet size metric would be inaccurate.
This capacity limit can be configured in NFD configuration file \path{tables.cs\_max\_packets} key, or via \texttt{Cs::setLimit} API in a simulation environment.
Runtime changes of the capacity limit are allowed.

NFD offers multiple policy implementations as subclasses of \texttt{nfd::cs::Policy}.
The effective policy can be configured in NFD configuration file \path{tables.cs\_policy} key, or via \texttt{Cs::setPolicy} API in a simulation environment.
This configuration can only be applied during initialization; runtime changes are disallowed.

\paragraph{\texttt{Policy} class API}

\texttt{nfd::cs::Policy} is the base class for all cache replacement policy implementations.

The capacity limit is stored on the \texttt{Policy} class, and can be changed via \texttt{Policy::setLimit} public method.
A pure virtual method \texttt{evictEntries} must be provided by a policy implementation to handle the lowering of the capacity limit by evicting enough entries to bring the CS back under the new capacity limit.
In addition to the main capacity limit (referred to as the ``hard limit''), a policy may offer additional capacity limits (such as a separate limit for certificates, or a packet size based limit).

In order for a policy to maintain the capacity limit, it needs to know what Data packets have been added to the cache and their access patterns.
Therefore, the policy class exposes four public methods to receive those information:
\begin{compactitem}
\item \textbf{Policy::afterInsert} is invoked after a new entry is inserted;
\item \textbf{Policy::afterRefresh} is invoked after an existing entry is refreshed by same Data;
\item \textbf{Policy::beforeErase} is invoked before an entry is erased via management command (currently unused);
\item \textbf{Policy::beforeUse} is invoked when an entry is found by a lookup and before it's used in forwarding.
\end{compactitem}
These public methods call into corresponding pure virtual methods: \texttt{doAfterInsert}, \texttt{doAfterRefresh}, \texttt{doBeforeErase}, \texttt{doBeforeUse}.
These pure virtual methods, as well as \texttt{evictEntries}, should be overridden in subclasses.

Based on information received via the above APIs, a policy maintains an internal cleanup index, which is used to determine which Data packet should be evicted when the CS exceeds the capacity limit.
The structure of this internal cleanup index is defined in each policy implementation.
It should reference CS entries (stored in the \emph{Table}) via iterators (\texttt{nfd::cs::iterator}).
When a policy decides to evict an entry, it should emit \texttt{beforeEvict} signal to inform the CS to erase the entry from the \emph{Table}, and then delete the corresponding item in the policy's internal cleanup index.
Note that \texttt{beforeErase} will not be invoked for entries evicted via \texttt{beforeEvict} signal.

The recommended procedures for each function are:
\begin{compactitem}
\item In \textbf{doAfterInsert}, the policy decides whether to accept the new entry.
      If it is accepted, the iterator of the new entry should be inserted into the internal cleanup index; otherwise, \texttt{cs::Policy::evictEntries} will be called to inform CS to do cleanup.
      Then, the policy should check whether CS size exceeds the capacity limit, and evict an entry (probably by calling \texttt{evictEntries} function) if so.
\item In \textbf{doAfterRefresh}, the policy may update its cleanup index to note that the same Data has arrived again.
\item In \textbf{doBeforeErase}, the policy should delete the corresponding item in its cleanup index.
\item In \textbf{doBeforeUse}, the policy may update its cleanup index to note that the indicated entry is accessed to satisfy an incoming Interest.
\item In \textbf{evictEntries}, the policy should evict enough entries so that the CS does not exceed capacity limit.
\end{compactitem}

\paragraph{Priority FIFO cache policy}

Priority-FIFO is the default cs::Policy.
Priority-FIFO evicts upon every insertion, because its performance is more predictable; the alternative, periodic cleanup of a batch of entries, can cause jitter in packet forwarding.
Priority-FIFO uses three queues to keep track of Data packets in CS:

\begin{compactitem}
\item \textbf{unsolicited queue} contains entries with unsolicited Data;
\item \textbf{stale queue} contains entries with stale Data;
\item \textbf{FIFO queue} contains entries with fresh Data.
\end{compactitem}

At any time, an entry belongs to exactly one queue, and can appear only once in that queue.
Priority-FIFO keeps which queue each entry belongs to.

These fields, along with the Table iterator stored in the queue, establish a bidirectional relation between the Table and the queues.

Mutation operations must maintain this relation:

\begin{compactitem}
\item When an entry is inserted, the Entry is emplaced in the Table, and 
\item When an entry is evicted, its Table iterator erased from the head of its queue, and the entry is erased from the Table.
\item When a fresh entry becomes stale (which is controlled by a timer), its Table iterator is moved from the FIFO queue to the stale queue, and the queue indicator and iterator on the entry are updated.
\item When an unsolicited/stale entry is updated with a solicited Data, its Table iterator is moved from the unsolicited/stale queue to the FIFO queue, and the queue indicator and iterator on the entry are updated
\end{compactitem}

A \textbf{queue}, despite the name, is not a real first-in-first-out queue, because an entry can move between queues (see mutation operations above).
When an entry is moved, its Table iterator is detached from the old queue, and appended to the new queue.
\texttt{std::list} is used as the underlying container; \texttt{std::queue} and \texttt{std::deque} are unsuitable because they can't efficiently detach a node.

\paragraph{LRU cache policy}

LRU cache policy implements the Least Recently Used cache replacement algorithm, which discards the least recently used items first.
LRU evicts upon every insertion, because its performance is more predictable; the alternative, periodic cleanup of a batch of entries, can cause jitter in packet forwarding.

LRU uses one queue to keep track of data usage in CS.
The Table iterator is stored in the queue.
At any time, when an entry is used or refreshed, its Table iterator is relocated to the tail of the queue. Also, when an entry is newly inserted, its Table iterator is pushed at the tail of the queue.
When an entry needs to be evicted, its Table iterator is erased from the head of its queue, and the entry is erased from the Table.

The \textbf{queue} uses \texttt{boost::multi\_index\_container}~\cite{boost-multi-index} as the underlying container due to its good performance in benchmarks.
\texttt{boost::multi\_index\_container::sequenced\_index} is used for inserting, updating usage and refreshing and \texttt{boost::\allowbreak multi\_index\_container::ordered\_unique\_index} is used for erasing by \texttt{Table::iterator}.

