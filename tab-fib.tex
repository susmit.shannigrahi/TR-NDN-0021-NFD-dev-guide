\subsection{Forwarding Information Base (FIB)}\label{sec:tab-fib}

The Forwarding Information Base (FIB) is used to forward Interest packets toward potential source(s) of matching Data \cite{Jacobson:2009:NNC:1658939.1658941}.
For each Interest that needs to be forwarded, a longest prefix match lookup is performed on the FIB, and the list of outgoing faces stored on the found FIB entry is an important reference for forwarding.

The structure, semantics, and algorithms of FIB is outlined in Section~\ref{sec:tab-fib-struct}.
How FIB is used by rest of NFD is described in Section~\ref{sec:tab-fib-usage}.
The implementation of FIB algorithms is discussed in Section~\ref{sec:tab-nt}.


\subsubsection{Structure and Semantics}\label{sec:tab-fib-struct}

Figure~\ref{fig:fib-struct} shows logical content and relationships between the FIB, FIB entries, and NextHop records.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.7]{fib-struct}
\caption{FIB and related entities}\label{fig:fib-struct}
\end{figure}

\paragraph{FIB entry and NextHop record}\

A FIB entry (\texttt{nfd::fib::Entry}) contains a name prefix and a non-empty collection of NextHop records.
A FIB entry of a certain prefix means that given an Interest under this prefix, a potential source(s) of matching Data can be reached via the faces given by the NextHop record(s) in this FIB entry.

Each NextHop record (\texttt{nfd::fib::NextHop}) contains an outgoing face toward a potential content source and its routing cost.
A FIB entry can contain at most one NextHop record toward the same outgoing face.
Within a FIB entry, NextHop records are ordered by ascending cost.
The routing cost is relative between NextHop records; the absolute value is insignificant.

Unlike the RIB (Section~\ref{sec:prefix-registration-flags}), there is no inheritance between FIB entries.
The NextHop records within a FIB entry are the only ``effective'' nexthops for this FIB entry.

\paragraph{FIB}\

The FIB (\texttt{nfd::Fib}) is a collection of FIB entries, indexed by name prefix.
The usual insertion, deletion, exact match operations are supported.
FIB entries can be iterated over in a forward iterator, in unspecified order.

\textbf{Longest Prefix Match} algorithm (\texttt{Fib::findLongestPrefixMatch}) finds a FIB entry that should be used to guide the forwarding of an Interest.
It takes a name as input parameter; this name should be the name field in an Interest.
The return value is a FIB entry such that its name prefix is
\begin{inparaenum}[(1)]
\item\label{tab-fib-lpm-prefix} a prefix of the parameter, and
\item the longest among those satisfying condition~\ref{tab-fib-lpm-prefix};
\end{inparaenum}
NULL is returned if no FIB entry satisfy condition~\ref{tab-fib-lpm-prefix}.

\texttt{Fib::removeNextHopFromAllEntries} is a convenient method that iterates over all FIB entries, and removes NextHop record of a certain face from every entry.
Since a FIB entry must contain at least one FIB entry, if the last NextHop record is removed, the FIB entry is deleted.
This is useful when a face is gone.


\subsubsection{Usage}\label{sec:tab-fib-usage}

The FIB is updated only through using FIB management protocol, which on NFD sides is operated by the FIB manager (Section~\ref{sec:mgmt-fib}).
Typically, FIB manager takes commands from RIB Management (Section~\ref{sec:rib}), which in turn receives static routes configured manually or registered by applications, and dynamic routes from routing protocols.
Since most FIB entries ultimately come from dynamic routes, the FIB is expected to contain a small number of entries, if the network has a small number of advertised prefixes.

The FIB is expected to be relatively stable.
FIB updates are triggered by RIB updates, which in turn is caused by manual configuration, application startup or shutdown, and routing updates.
These events are infrequent in a stable network.
However, each RIB update can cause lots of FIB updates, because changes in one RIB entry may affect its descendants due to inheritance.

The longest prefix match algorithm is used by forwarding in \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}).
It is called at most once per incoming Interest.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
