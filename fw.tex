\section{Forwarding}\label{sec:fw}

The packet processing in NFD consists of \textbf{forwarding pipelines}, described in this section, and \textbf{forwarding strategies} described in Section~\ref{sec:strategy}.
A \textbf{forwarding pipeline} (or just pipeline) is a series of steps that operates on a packet or a PIT entry, and is triggered by a specific event: reception of the Interest, detecting that the received Interest was looped, when an Interest is ready to be forwarded out of a face, etc.
A \textbf{forwarding strategy} (or just strategy) is a decision maker about Interest forwarding, and is attached at the beginning or end of the pipelines.
In other words, the strategy makes decisions on whether, when, and where to forward an Interest, while the pipelines supply the strategy the Interests and supporting information to make these decisions.

Figure~\ref{fig:fw-overall} shows the overall workflow of forwarding pipelines and strategy, where blue boxes represent pipelines and white boxes represent decision points of the strategy.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.75]{fw-overall}
\caption{Pipelines and strategy: overall workflow}\label{fig:fw-overall}
\end{figure}


\subsection{Forwarding Pipelines}\label{sec:fw-pipelines}

The pipelines operate on network layer packets (Interest, Data, or Nack) and each packet is passed from one pipeline to another (in some cases through strategy decision points) until all processing is finished.
Processing within pipelines uses CS, PIT, Dead Nonce list, FIB, network region table, and Strategy Choice table, however for the last three pipelines have only read-only access, as those tables are managed by the corresponding managers and are not directly affected by data plane traffic.

\texttt{FaceTable} keeps track all active faces in NFD.
It is the entry point from which an incoming network layer packet is given to the forwarding pipelines for processing.
Pipelines are also allowed to send packets through faces.

The processing of Interest, Data, and Nack packets in NDN is quite different.
We separate forwarding pipelines into \textbf{Interest processing path}, \textbf{Data processing path}, and \textbf{Nack processing path}, described in the following sections.


\subsection{Interest Processing Path}\label{sec:fw-interest}

NFD separates Interest processing into the following pipelines:

\begin{itemize}
\item Incoming Interest: processing of incoming Interests
\item Interest loop: processing of incoming looped Interests
\item ContentStore hit: processing of incoming Interests that can be satisfied by cached Data
\item ContentStore miss: processing of incoming Interests that cannot be satisfied by cached Data
\item Outgoing Interest: preparation and sending out of Interests
\item Interest reject: processing of PIT entries that are rejected by the strategy
\item Interest unsatisfied: processing of PIT entries that are unsatisfied before all downstreams timeout
\item Interest finalize: deleting PIT entries
\end{itemize}

\subsubsection{Incoming Interest Pipeline}\label{sec:fw-incoming-interest}

The incoming Interest pipeline is implemented in \texttt{Forwarder::onIncomingInterest} method and is entered from \texttt{Forwarder::\allowbreak startProcessInterest} method, which is triggered by \texttt{Face::\allowbreak afterReceiveInterest} signal.
The input parameters to the incoming interest pipeline include the newly received Interest packet and reference to the Face on which this Interest packet was received.

This pipeline includes the following steps, summarized in Figure~\ref{fig:fw-incoming-interest}:

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{fw-incoming-interest}
\caption{Incoming Interest pipeline}\label{fig:fw-incoming-interest}
\end{figure}

\begin{enumerate}

\item The first step is to check for \texttt{/localhost} scope~\cite{ScopeControl} violation.
In particular, an Interest from a non-local face is not allowed to have a name that starts with \texttt{/localhost} prefix, as it is reserved for localhost communication.
If a violation is detected, the Interest is immediately dropped and no further processing is performed.
This check guards against malicious senders; a compliant forwarder will never send a \texttt{/localhost} Interest to a non-local face.
Note that \texttt{/localhop} scope is not checked here, because its scope rules do not restrict incoming Interests.

\item The Name and Nonce of the incoming Interest are checked against the Dead Nonce List (Section~\ref{sec:tab-dnl}).
If a match is found, the incoming Interest is suspected of a loop, and is given to the \textit{Interest loop pipeline} for further processing (Section~\ref{sec:fw-interest-loop}).
If a match is not found, processing continues onto the next step.
Note that, unlike a duplicate Nonce detected with PIT entry (described below), a duplicate detected by the Dead Nonce List does not cause the creation of a PIT entry, because creating an in-record for this incoming Interest would cause matching Data, if any, to be returned to the downstream, which is incorrect; on the other hand, creating a PIT entry without an in-record is not helpful for future duplicate Nonce detection.

\item If the Interest carries a Link object, the procedure determines whether the Interest has reached the producer region, by checking if any delegation name in the Link object is a prefix of any region name from the \textit{network region table} (Section~\ref{sec:tab-nrt}).
      If so, the Link object and SelectedDelegation fields are removed, as they have completed their mission of bringing the Interest into the producer region, and are no longer necessary.

\item The next step is looking up existing or creating a new PIT entry, using name and selectors specified in the Interest packet.
As of this moment, the PIT entry becomes a processing subject of the incoming Interest and following pipelines.
Note that NFD creates the PIT entry before performing ContentStore lookup.
The main reason for this decision is to reduce the lookup overhead, on the assumption that the ContentStore is likely to be significantly larger than the PIT, because in some cases described below CS lookup can be skipped.

\item Before the incoming Interest is processed any further, its Nonce is checked against the Nonces in the PIT entry and the Dead Nonce List (Section~\ref{sec:tab-dnl}).
If a match is found, the incoming Interest is considered a duplicate due to either loop or multi-path arrival, and is given to \textit{Interest loop pipeline} for further processing (Section~\ref{sec:fw-interest-loop}).
If a match is not found, processing continues.

\item Next, the \textit{unsatisfy timer} and \textit{straggler timer} (Section~\ref{sec:fw-interest-reject}) on the PIT entry are cancelled, because a new valid Interest has arrived, so the lifetime of the PIT entry needs to be extended.
The timers may be reset later on in the Interest processing path, for example if the ContentStore is able to satisfy the Interest.

\item The pipeline then tests whether the Interest is pending, i.e., if the PIT entry already has another in-record from the same or another incoming Face.
Recall that NFD's PIT entry can represent not only a pending Interest but also a recently satisfied Interest (Section~\ref{sec:tab-pit-entry}).
This test is equivalent to ``having a PIT entry'' in CCN Node Model \cite{Jacobson:2009:NNC:1658939.1658941}, whose PIT contains only pending Interests.

\item If the Interest is not pending, the Interest is matched against the ContentStore (\texttt{Cs::find}, Section~\ref{sec:tab-cs-api}).
Otherwise, CS lookup is unnecessary because a pending Interest implies that a previous CS returns no match.
Depending on whether there's a match in CS, Interest processing continues either in \textit{ContentStore miss pipeline} (Section~\ref{sec:fw-cs-miss}) or in \textit{ContentStore hit pipeline} (Section~\ref{sec:fw-cs-hit}).

\end{enumerate}

\subsubsection{Interest Loop Pipeline}\label{sec:fw-interest-loop}

This pipeline is implemented in \texttt{Forwarder::onInterestLoop} method and is entered from \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) when an Interest loop is detected.
The input parameters to this pipeline include an Interest packet, and its incoming Face.

This pipeline sends a Nack with reason code Duplicate to the Interest incoming face, if it's a point-to-point face.
Since the semantics of Nack is undefined on a multi-access link, if the incoming face is multi-access, the looping Interest is simply dropped.

\subsubsection{ContentStore Hit Pipeline}\label{sec:fw-cs-hit}

This pipeline is implemented in \texttt{Forwarder::onContentStoreMiss} method and is entered after \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) performs a ContentStore lookup (Section~\ref{sec:tab-cs-api}) and there's a match.
The input parameters to this pipeline include an Interest packet, its incoming Face, the PIT entry, and the matched Data packet.

This pipeline sets the \textit{straggler timer} (Section~\ref{sec:fw-interest-reject}) on the Interest because because it's being satisfied, and then passes the matching Data to \textit{outgoing Data pipeline} (Section~\ref{sec:fw-outgoing-data}).
Processing for this Interest is completed.

\subsubsection{ContentStore Miss Pipeline}\label{sec:fw-cs-miss}

This pipeline is implemented in \texttt{Forwarder::onContentStoreMiss} method and is entered after \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) performs a ContentStore lookup (Section~\ref{sec:tab-cs-api}) and there's no match.
The input parameters to this pipeline include an Interest packet, its incoming Face, and the PIT entry.

When this pipeline is entered, the Interest is valid and cannot be satisified by cached Data, so it needs to be forwarded somewhere.
This pipeline takes the following steps, summarized in Figure~\ref{fig:fw-cs-miss}:

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{fw-cs-miss}
\caption{ContentStore Miss pipeline}\label{fig:fw-cs-miss}
\end{figure}

\begin{enumerate}
\item An in-record for the Interest and its incoming face is inserted into the PIT entry.
      In case an in-record for the same incoming face already exists (i.e., the Interest is being retransmitted by the same downstream), it's simply refreshed with the newly observed Interest Nonce and expiration time.
      The expiration time of this in-record is controlled by the \texttt{InterestLifetime} field in the Interest packet; if \texttt{InterestLifetime} is omitted, the default 4~seconds is used.
\item The \textit{unsatisfy timer} on the PIT entry is set to expire when all in-records in the PIT entry expire.
      When the unsatisfy timer expires, the \textit{Interest unsatisfied pipeline} (Section~\ref{sec:fw-interest-unsatisfied}) is executed.
\item If the Interest carries a \texttt{NextHopFaceId} field in its NDNLPv2 header, the pipeline honors this field.
      The chosen next hop face is looked up in the FaceTable.
      If a face is found, the \textit{outgoing Interest pipeline} (Section~\ref{sec:fw-outgoing-interest}) is executed;
      if the face does not exist, the Interest is dropped.
\item Without a \texttt{NextHopFaceId} field, a forwarding strategy is responsible for making forwarding decision on the Interest.
      Therefore, the pipeline invokes Find Effective Strategy algorithm (Section~\ref{sec:tab-sc-struct}) to determine which strategy to use, and invokes the \textit{after receive Interest} trigger of the chosen strategy with the Interest packet, its incoming face, and the PIT entry (Section~\ref{sec:strategy-after-receive-interest}).
\end{enumerate}

Note that forwarding defers to the strategy the decision on whether, when, and where to forward an Interest.
Most strategies forward a new Interest immediately to one or more upstreams found through a FIB lookup.
For a retransmitted Interest, most strategies will suppress it if the previous forwarding happened recently (see Section~\ref{sec:strategy-after-receive-interest} for more details), and forward it otherwise.

\subsubsection{Outgoing Interest Pipeline}\label{sec:fw-outgoing-interest}

The outgoing Interest pipeline is implemented in \texttt{Forwarder::\allowbreak onOutgoingInterest} method and is entered from \texttt{Strategy::\allowbreak sendInterest} method which handles \textit{send Interest action} for strategy (Section~\ref{sec:strategy-send-interest}).
The input parameters to this pipeline include a PIT entry, an outgoing Face, and the Interest packet.
Note that the Interest packet is not a parameter when entering the pipeline.
The pipeline steps either use the PIT entry directly to perform checks, or obtain a reference to an Interest stored inside the PIT entry.

This pipeline first inserts an out-record in the PIT entry for the specified outgoing face, or update an existing out-record for the same face; in either case, the PIT out-record remembers the Nonce of the last outgoing Interest packet, which is useful for matching incoming Nacks, as well as an expiration timestamp which is the current time plus InterestLifetime.
Finally, the Interest is sent to the outgoing face.

\subsubsection{Interest Reject Pipeline}\label{sec:fw-interest-reject}

This pipeline is implemented in \texttt{Forwarder::\allowbreak onInterestReject} method and is entered from \texttt{Strategy::\allowbreak re\-jectPen\-dingIn\-te\-rest} method which handles \textit{reject pending Interest action} for strategy (Section~\ref{sec:strategy-reject-pending-interest}).
The input parameters to this pipeline include a PIT entry.

The pipeline cancels the \textit{unsatisfy timer} on the PIT entry (set by \textit{incoming Interest pipeline}), and then sets the \textit{straggler timer}.
After the straggler timer expires, Interest finalize pipline (Section~\ref{sec:fw-interest-finalize}) is entered.

The purpose of the straggler timer is to keep the PIT entry alive for a short period of time in order to facilitate duplicate Interest detection and to collect data plane measurements.
For duplicate Interest detection this is necessary, since NFD uses the Nonces stored inside PIT entry to remember recently seen Interest Nonces.
For data plane measurements it is desirable to obtain as many data points as possible, e.g., if several incoming Data packets can satisfy the pending Interest, all of these Data packets should be used to measure performance of the data plane.
If the PIT entry is deleted right away, NFD may fail to properly detect Interest loops and valuable measurements can be lost.

We chose 100~ms as a static value for the straggler timer, as we believe it provides a good tradeoff between functionality and memory overhead:
for loop detection purposes, this time is enough for most packets to go around a cycle; for measurement purposes, a working path that is more than 100~ms slower than the best path is usually not useful.
If necessary, this value can be adjusted in \verb|daemon/fw/forwarder.cpp| file.

\subsubsection{Interest Unsatisfied Pipeline}\label{sec:fw-interest-unsatisfied}

This pipeline is implemented in \texttt{Forwarder::onInterestUnsatisfied} method and is entered from the \textit{unsatisfy timer} (Section~\ref{sec:fw-incoming-interest}) when InterestLifetime expires for all downstreams.
The input parameters to this pipeline include a PIT entry.

The processing steps in the Interest unsatisfied pipeline are:

\begin{enumerate}
\item Determining the strategy that is responsible for the PIT entry using Find Effective Strategy algorithm on the Strategy Choice table (see Section~\ref{sec:tab-sc-struct}).
\item Invoking \textit{before expire Interest} action of the effective strategy with the PIT entry as the input parameter (Section~\ref{sec:strategy-before-expire-interest}).
\item Entering Interest finalize pipeline (Section~\ref{sec:fw-interest-finalize}).
Note that at this stage there is no need to keep the PIT entry alive any longer, as it is the case in the Interest reject pipeline (Section~\ref{sec:fw-interest-reject}).
Expiration of the unsatisfy timer implies that PIT entry was already alive for a substantial period of time:
all Interest loops have already been prevented, and no matching Data have been received.
\end{enumerate}

\subsubsection{Interest Finalize Pipeline}\label{sec:fw-interest-finalize}

This pipeline is implemented in \texttt{Forwarder::onInterestFinalize} method and is entered from the \textit{straggler timer} (Section~\ref{sec:fw-interest-reject}) or Interest unsatisfied pipeline (Section~\ref{sec:fw-interest-unsatisfied}).

The pipeline first determines whether any Nonces recorded in the PIT entry need to be inserted into the Dead Nonce List (Section~\ref{sec:tab-dnl}).
The Dead Nonce List is a global data structure designed to detect looping Interests, and we want to insert as few Nonces as possible to keep its size down.
Only outgoing Nonces (in out-records) need to be inserted, because an incoming Nonce that has never been sent out cannot loop back.

We can take further chances on the ContentStore: if the PIT entry is satisfied, and the ContentStore can satisfy a looping Interest (thus stop the loop) during \textit{Dead Nonce List entry lifetime} if Data packet isn't evicted, Nonces in this PIT entry don't need to be inserted.
The ContentStore is believed to be able to satisfy a looping Interest, if the Interest does not have MustBeFresh selector, or the cached Data's FreshnessPeriod is no less than \textit{Dead Nonce List entry lifetime}.

If it is determined that one or more Nonces should be inserted into the Dead Nonce List, tuples of Name and Nonce are added to the Dead Nonce List (Section~\ref{sec:tab-dnl-sem}).

Finally, the PIT entry is removed from the PIT.


\subsection{Data Processing Path}

Data processing in NFD is split into these pipelines:

\begin{itemize}
\item Incoming Data: processing of incoming Data packets
\item Data unsolicited: processing of incoming unsolicited Data packets
\item Outgoing Data: preparation and sending out of Data packets
\end{itemize}

\subsubsection{Incoming Data Pipeline}\label{sec:fw-incoming-data}

The incoming Data pipeline is implemented in \texttt{Forwarder::onIncomingData} method and is entered from \texttt{Forwarder::\allowbreak startProcessData} method, which is triggered by \texttt{Face::\allowbreak afterReceiveData} signal.
The input parameters to this pipeline include a Data packet and its incoming Face.

This pipeline includes the following steps, summarized in Figure~\ref{fig:fw-incoming-data}:

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{fw-incoming-data}
\caption{Incoming Data pipeline}\label{fig:fw-incoming-data}
\end{figure}

\begin{enumerate}

\item Similar to the incoming Interest pipeline, the first step in the incoming Data pipeline is to check the Data packet for violation of \texttt{/localhost} scope~\cite{ScopeControl}.
If the Data comes from a non-local Face, but the name begins with \texttt{/localhost} prefix, the scope is violated, Data packet is dropped, and further processing is stopped.

This check guards against malicious senders; a compliant forwarder will never send a \texttt{/localhost} Data to a non-local Face.
Note that \texttt{/localhop} scope is not checked here, because its scope rules do not restrict incoming Data.

\item After name-based scope constraint is checked, the Data packet is matched against the PIT using Data Match algorithm  (Section~\ref{sec:tab-pit-table}).
If no matching PIT entry is found, the Data is unsolicited, and is given to \textit{Data unsolicited pipeline} (Section~\ref{sec:fw-data-unsolicited}).

\item If one or more matching PIT entries are found, the Data is inserted into the ContentStore.
Note that even if the pipeline inserts the Data to the ContentStore, whether it is stored and how long it stays in the ContentStore is determined by ContentStore admission and replacement policy.%
\footnote{The current implementation has a fixed ``admit all'' admission policy, and ``priority FIFO'' as replacement policy, see Section~\ref{sec:tab-cs}.}

\item The next step is to cancel the \textit{unsatisfy timer} (Section~\ref{sec:fw-incoming-interest}) and the \textit{straggler timer} (Section~\ref{sec:fw-interest-reject}) for each found PIT entry, because the pending Interest is now being satisfied.

\item Next, the effective strategy responsible for the PIT entry is determined using Find Effective Strategy algorithm (Section~\ref{sec:tab-sc-struct}).
The selected strategy is then triggered for the \textit{before satisfy Interest} action with the PIT entry, the Data packet, and its incoming Face (Section~\ref{sec:strategy-before-satisfy-interest}).

\item The Nonce on the PIT out-record corresponding to the incoming Face of the Data is inserted to the Dead Nonce List (Section~\ref{sec:tab-dnl}), if it's deemed necessary (Section~\ref{sec:fw-interest-finalize}).
This step is necessary because the next step would delete the out-record and the outgoing Nonce would be lost.

\item The PIT entry is marked satisfied by deleting all in-records and the out-record corresponding to the incoming Face of the Data.

\item The \textit{straggler timer} (Section~\ref{sec:fw-interest-reject}) is started on the PIT entry.

\item Finally, for each pending downstream except the incoming Face of this Data packet, \textit{outgoing Data pipeline} (Section~\ref{sec:fw-outgoing-data}) is entered with the Data packet and the downstream Face as arguments.
Note that this happens only once for each downstream, even if it appears in multiple PIT entries.
To implement this, during the processing of matched PIT entries as described above, NFD collects their pending downstreams into an unordered set, eliminating all potential duplicates.

\end{enumerate}

\subsubsection{Data Unsolicited Pipeline}\label{sec:fw-data-unsolicited}

This pipeline is implemented in \texttt{Forwarder::onDataUnsolicited} method and is entered from the \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) when a Data packet is found to be unsolicited.
The input parameters to this pipeline include a Data packet, and its incoming Face.

The current \textit{unsolicited Data policy} is consulted to decide whether to drop the Data, or add it to the ContentStore.
By default, NFD forwarding is configured with a ``drop-all'' policy which drops all unsolicited Data, as they pose a security risk to the forwarder.

There can be cases where unsolicited Data packets need to be accepted.
The policy can be changed in NFD configuration file at \texttt{tables.cs\_unsolicited\_policy} key.

\subsubsection{Outgoing Data Pipeline}\label{sec:fw-outgoing-data}

This pipeline is implemented in \texttt{Forwarder::onOutgoingData} method and pipeline is entered from \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}) when a matching Data is found in ContentStore and from \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) when the incoming Data matches one or more PIT entries.
The input parameters to this pipeline include a Data packet, and the outgoing Face.

This pipeline contains the following steps:

\begin{enumerate}
\item The Data is first checked for \texttt{/localhost} scope~\cite{ScopeControl}:
Data packets with a \texttt{/localhost} prefix cannot be sent to a non-local face.%
\footnote{This check is only useful in a specific scenario (see NFD Bug 1644).}
\texttt{/localhop} scope is not checked here, because its scope rules do not restrict outgoing Data.
\item The next step is reseved for the traffic manager actions, such as to perform traffic shaping, etc.
The current version does not include any traffic management, but it is planned to be implemented in a future release.
\item Finally, the Data packet is sent via the outgoing Face.
\end{enumerate}


\subsection{Nack Processing Path}\label{sec:fw-nack}

Nack processing in NFD is split into these pipelines:

\begin{itemize}
\item Incoming Nack: processing of incoming Nacks
\item Outgoing Nack: preparation and sending out of Nacks
\end{itemize}

\subsubsection{Incoming Nack Pipeline}\label{sec:fw-incoming-nack}

The incoming Nack pipeline is implemented in \texttt{Forwarder::onIncomingNack} method and is entered from \texttt{Forwarder::\allowbreak startProcessNack} method, which is triggered by \texttt{Face::\allowbreak afterReceiveNack} signal.
The input parameters to this pipeline include a Nack packet and its incoming Face.

First, if the incoming face is a multi-access face, the Nack is dropped without further processing, because the semantics of Nack on a multi-access link is undefined.

The Interest carried in the Nack and its incoming face is used to locate a PIT out-record for the same face where the Interest has been forwarded to, and the last outgoing Nonce was same as the Nonce carried in the Nack.
If such an out-record is found, it's marked \textit{Nacked} with the Nack reason.
Otherwise, the Nack is dropped because it's no longer relevant.

The effective strategy responsible for the PIT entry is determined using Find Effective Strategy algorithm (Section~\ref{sec:tab-sc-struct}).
The selected strategy is then triggered for the \textit{after receive Nack} procedure with the Nack packet, its incoming Face, and the PIT entry (Section~\ref{sec:strategy-after-receive-nack}).

\subsubsection{Outgoing Nack Pipeline}\label{sec:fw-outgoing-nack}

The outgoing Nack pipeline is implemented in \texttt{Forwarder::onOutgoingNack} method and is entered from \texttt{Strategy::\allowbreak sendNack} method which handles \textit{send Nack action} for strategy (Section~\ref{sec:strategy-send-nack}).
The input parameters to this pipeline include a PIT entry, an outgoing Face, and the Nack header.

First, the PIT entry is queried for an in-record of the specified outgoing face (downstream).
This in-record is necessary because protocol requires the last Interest received from the downstream, including its Nonce, to be carried in the Nack packet.
If no in-record is found, abort this procedure, because the Nack cannot be sent without this Interest.

Second, if the downstream is a multi-access face, abort this procedure, because the semantics of Nack on a multi-access link is undefined.

After both checks are passing, a Nack packet is constructed with the provided Nack header and the Interest from the in-record, and sent through the face.
The in-record is erased as it has been ``satisfied'' by the Nack, and no further Nack or Data should be sent to the same downstream unless there's a retransmission.


\subsection{Helper Algorithms}\label{sec:fw-helper}

Several algorithms used in forwarding pipelines and multiple strategies are implemented as helper functions.
As we identify more reusable algorithms, they will be implemented as helper functions as well, rather than repeating the code in several places.

\texttt{nfd::fw::wouldViolateScope} determines whether forwarding an Interest out of a face would violate namespace-based scope control.

\texttt{nfd::fw::findDuplicateNonce} searches a PIT entry to see if there's a duplicate Nonce in any in-record or out-record.

\texttt{nfd::fw::hasPendingOutRecords} determines whether a PIT entry has an out-record that is still pending, i.e. neither Data nor Nack has come back.

\subsubsection{FIB lookup}\label{sec:fw-fib-lookup}

\texttt{Strategy::lookupFib} implements a FIB lookup procedure with consideration of Link object.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\linewidth]{fw-fib-lookup}
\caption{FIB lookup procedure}\label{fig:fw-fib-lookup}
\end{figure}

The procedure (Figure~\ref{fig:fw-fib-lookup}) is:

\begin{enumerate}
\item If the Interest does not carry a Link object and hence does not require mobility processing, FIB is looked up using Interest Name (Section~\ref{sec:tab-fib-struct}, Longest Prefix Match algorithm).
      FIB guarantees that Longest Prefix Match returns a valid FIB entry; however, a FIB entry may contain empty set of NextHop records, which could effectively result (but, strictly speaking, is not required to happen) in the strategy rejecting the Interest.
\item If the Interest carries a Link object, it is processed for mobility support. \footnote{Presence of a Link object at this point indicates the Interest has not reached the producer region, because Link object should have been stripped in \textit{incoming Interest pipeline} when entering the producer region.}
\item The procedure inspects whether the Interest contains the SelectedDelegation field, which indicates that a downstream forwarder has chosen which delegation to use.
      If so, it implies that the Interest is already in default-free zone, FIB lookup is performed using the selected delegation name.
\item The procedure determines whether the Interest is in the consumer region or has reached the default-free zone, by looking up the first delegation Name in FIB.
      If this lookup turns out the default route (i.e., the root FIB entry \texttt{ndn:/} with a non-empty nexthop list), it means the Interest is still in consumer region, and this lookup result is passed down to next step.
      Otherwise, the Interest has reached the default-free zone.
\item The procedure selects a delegation for an Interest that has reached the default free zone, by looking up every delegation name in the FIB, and choose the lowest-cost delegation that matches a non-empty FIB entry.
      The chosen delegation is written into the SelectedDelegation field in the Interest packet, so that upstream forwarders can follow this selection without doing multiple FIB lookups.
\end{enumerate}

A limitation of current implementation is that, when an Interest reaches the first default-free router, which delegation to use is solely determined by this FIB lookup procedure according to the routing cost in the FIB.
Ideally, this choice should be made by the strategy which can take current performance of different upstreams into consideration.
We are exploring a better design in this aspect.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
