\subsubsection{Best Route Strategy}\label{sec:strategy-best-route}

The best route strategy forwards an Interest to the upstream with lowest routing cost.
This strategy is implemented as \texttt{nfd::fw::BestRouteStrategy2} class.

% VERSION 1 BEHAVIOR

% The strategy forwards new Interests only; if an Interest is not new (unexpired out-record exists), it is not forwarded.
% For a new Interest, the list of nexthop records is consulted to find an \textit{eligible} (Section~\ref{sec:strategy-multicast}) upstream with lowest routing cost, and the Interest is forwarded to that face.
% If there is no eligible upstream, the Interest is rejected.

\paragraph{Interest forwarding}

The strategy forwards a new Interest to the lowest-cost nexthop (except downstream).
After the new Interest is forwarded, a similar Interest with same Name, Selectors, and Link but different Nonce would be suppressed if it's received during a retransmission suppression interval.
An Interest received after the suppression interval is called a ``retransmission'', and is forwarded to the lowest-cost nexthop (except downstream and those that would violate namespace-based scope control) that is not previously used; if all nexthops have been used, it is forwarded to a nexthop that was used earliest.

It's worth noting that the suppression and retransmission mechanism does not distiguish between an Interest from the same downstream and an Interest from a different downstream.
Although the former is typically a retransmission from the same consumer and the latter is typically from a different consumer making use of NDN's built-in Data multicast, there's no prominent difference in terms of forwarding, so they are processed alike.

\paragraph{Retransmission suppression interval}

Instead of forwarding every incoming Interest, the retransmission suppression interval is imposed to prevent a malicious or misbehaving downstream from sending too many Interests end-to-end.
The retransmission suppression interval should be chosen so that it permits reasonable consumer retransmissions, while prevents DDoS attacks by overly frequent retransmissions.

We have identified three design options for setting the retransmission suppression interval:

\begin{asparaitem}
\item A \textbf{fixed interval} is the simplest, but it's hard to find a balance between reasonable consumer retransmissions and DDoS prevention.
\item Doing \textbf{RTT estimation} would allow a retransmission after the strategy believes the previous forwarded Interest is lost or otherwise won't be answered, but RTT estimations aren't reliable, and in case the consumer applications are also using RTT estimation to schedule their retransmissions, this results in double control loop and potentially unstable behavior.
\item Using \textbf{exponential back-off} gives consumer applications control over the retransmission, and also effectively prevents DDoS. Starting with a short interval, the consumer can retransmit quickly in low RTT communication scenario; the interval goes up after each accepted retransmission, so an attacker cannot abuse the mechanism by retransmitting too frequently.
\end{asparaitem}

We finally settled with the exponential back-off algorithm.
The initial interval is set to 10 milliseconds.
After each retransmission being forwarded, the interval is doubled (multiplied by $2.0$), until it reaches a maximum of 250 milliseconds.

\paragraph{Nack generation}

The best route strategy uses Nack to improve its performance.

When a new Interest is to be forwarded in \textit{after receive Interest} trigger (Section~\ref{sec:strategy-after-receive-interest}), but there's no eligible nexthop, a Nack will be returned to the downstream.
A nexthop is \textit{eligible} if it is not same as the downstream of current incoming Interest, and forwarding to this nexthop does not violate scope \cite{ScopeControl}.
TODO\#3420 add ``face is UP'' condition.
If there's no eligible nexthop available, the strategy rejects the Interest, and returns a Nack to the downstream with reason \textit{no route}.

Currently, the Nack packet does not indicate which prefix that the node has no route to reach, because it's non-trivial to compute this prefix.
Also, Nacks will not be returned to multicast downstream faces.

\paragraph{Nack processing}

Upon receiving an incoming Nack, the strategy itself does not retry the Interest with other nexthops (because ``best route'' forwards to only one nexthop for each incoming Interest), but informs the downstream(s) as quickly as possible.
If the downstream/consumer wants, it can retransmit the Interest, and the strategy would retry it with another nexthop.

Specifically, depending on the situation of other upstreams, the strategy takes one of these actions:

\begin{asparaitem}
\item If all pending upstreams have Nacked, a Nack is sent to all downstreams.
\item If all but one pending upstream have Nacked, and that upstream is also a downstream, a Nack is sent to that downstream.
\item Otherwise, the strategy continues waiting for the arrival of more Nacks or Data.
\end{asparaitem}

To determine what situation a PIT entry is in, the strategy makes use of the \textit{Nacked} field (Section~\ref{sec:tab-pit-entry}) on PIT out-records, and does not require extra measurement storage.

The second situation, ``all but one pending upstream have Nacked and that upstream is also a downstream'', is introduced to address a specific ``live deadlock'' scenario, where two hosts are waiting for each other to return the Nack.
More details about this scenario can be found at \url{http://redmine.named-data.net/issues/3033#note-7}.

In the first situation, the Nack returned to downstreams need to indicate a reason.
In the easy case where there's only one upstream, the reason from this upstream is passed along to downstream(s).
When there are multiple upstreams, the \textbf{least severe} reason is passed to downstream(s), where the severity of Nack reasons are defined as: Congestion \textless Duplicate \textless NoRoute.
For example, one upstream has returned Nack-NoRoute and the other has returned Nack-Congestion.
This forwarder choose to tell downstream(s) ``congestion'' so that they can retry with this path after reducing their Interest sending rate, and this forwarder can forward the retransmitted Interests to the second upstream at a slower rate and hope it's no longer congested.
If we instead tell downstream(s) ``no route'', it would make downstreams believe that this forwarder cannot reach the content source at all, which is inaccurate.
