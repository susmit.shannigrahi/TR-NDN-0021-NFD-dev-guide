

\section{Security}
\label{sec:security}

Security consideration of NFD involves two parts: interface control and trust models.

\subsection{Interface Control}

The default NFD configuration requires superuser privileges to access raw ethernet interfaces and the Unix socket location.
Due to the research nature of NFD, users should be aware of the security risks and consequences of running as the superuser.

It is also possible to configure NFD to run without elevated privileges, but this requires disabling ethernet faces and changing the default Unix socket location\footnote{libndn-cxx expects the default Unix socket location, but this can be changed in the library's client.conf configuration file.} (both in the NFD configuration file, see Section~\ref{sec:core-config}).
However, such measures may be undesirable (e.g. performing ethernet-related development).
As a middle ground, users can also configure an alternate effective user and group id for NFD to drop privileges to when they are not needed.
This does not provide any real security benefit over running exclusively as the superuser, but it could potentially buggy code from damaging the system (see Section~\ref{sec:core-config}). 

\subsection{Trust Model}

Different trust models are used to validate command Interests depending on the recipient.
Among the four types of commands in NFD, the commands of {\tt faces}, {\tt fib}, and {\tt strategy-choice} are sent to NFD, while {\tt rib} commands are sent to the RIB Manager.

\subsubsection{Command Interest}

Command Interests are a mechanism for issuing authenticated control commands.
Signed commands are expressed in terms of a command Interest's name.
These commands are defined to have five additional components after the management namespace: command name, timestamp, random-value, SignatureInfo, and SignatureValue.
\begin{center}
{\tt /signed/interest/name/$<$timestamp$>$/$<$nonce$>$/$<$signatureInfo$>$/$<$signatureValue$>$}
\end{center}

The command Interest components have the following usages:

\begin{itemize} \itemsep -2pt
\item {\tt timestamp} is used to protect against replay attack.
\item {\tt nonce} is a random value (32 bits) which adds additional assurances that the command Interest will be unique.
\item {\tt signatureInfo} encodes a SignatureInfo TLV block.
\item {\tt signatureValue} encodes the a SignatureBlock TLV block.
\end{itemize}

A command interest will be treated as invalid in the following four cases:

\begin{itemize} \itemsep -2pt
\item one of the four components above (SignatureValue, SignatureInfo, nonce, and Timestamp) is missing or cannot be parsed correctly;
\item the key, according to corresponding trust model, is not trusted for signing the control command;
\item the signature cannot be verified with the public key pointed to by the KeyLocator in SignatureInfo;
\item the producer has already received a valid signed Interest whose timestamp is equal or later than the timestamp of the received one.
\end{itemize}

Note that in order to detect the fourth case, the producer needs to maintain a latest timestamp state for each trusted public key\footnote{Since public key cryptography is used, sharing private keys is not recommended.
If private key sharing is inevitable, it is the key owner's responsibility to keep clock synchronized.}
For each trusted public key, the state is initialized as the timestamp of the first valid Interest signed by the key.
Afterwards, the state will be updated each time the producer receives a valid command Interest.

Note that there is no state for the first command Interest.
To handle this special situation, the producer should check the Interest's timestamp against a proper interval (e.g., 120 seconds):
\[
 [current\_timestamp - interval/2, current\_timestamp + interval/2].
\]

The first Interest is invalid if its timestamp is outside of the interval.

\subsubsection{NFD Trust Model}

With the exception of the RIB Manager, NFD uses a simple trust model of associating privileges with NDN identity certificates.
There are currently three privileges that can be directly granted to identities: {\tt faces}, {\tt fib}, and {\tt strategy-choice}.
New managers can add additional privileges via the {\tt ManagerBase} constructor.

A command Interest is unauthorized if the signer's identity certificate is not associated with the command type.
Note that key retrievals are not permitted/performed by NFD for this trust model; an identity certificate is either associated with a privilege (authorized) or not (unauthorized).
For details about how to set privileges for each user, please see Section~\ref{sec:core} and Section~\ref{sec:mgmt}.


\subsubsection{NFD RIB manager Trust Model}

RIB manager uses its own trust model to authenticate {\tt rib} type command Interests. 
Applications that want to register a prefix in NFD (i.e., receive Interests under a prefix) may need to send an appropriate {\tt rib} command Interest.
After RIB manager authenticates the {\tt rib} command Interest, RIB manager will issue {\tt fib} command Interests to NFD to set up FIB entries.

Trust model for NFD RIB manager defines the conditions for keys to be trusted to sign {\tt rib} commands.
Namely, the trust model must answer two questions:

\begin{enumerate} \itemsep -2pt
\item Who are trusted signers for {\tt rib} command Interests?
\item How do we authenticate signers?
\end{enumerate}


%The answer to the first question is simplified as the requirements to the name of the signing key and can be expressed using
Trusted signers are identified by expressing the name of the signing key with a
\href{http://redmine.named-data.net/projects/ndn-cxx/wiki/Regex}{NDN Regular Expression}~\cite{ndnRegex}.
If the signing key's name does not match the regular expression, the command Interest is considered to be invalid.
Signers are authenticated by a rule set that explicitly specifies how a signing key can be validated via
a chain of trust back to a trust anchor
% The answer to the second question is a set of rules which explicitly specify how a signing key can be validated a long a chain of trust back to a trust anchor.
Both Signer identification and authentication can be specified in a configuration file that follows the \href{http://named-data.net/doc/ndn-cxx/current/tutorials/security-validator-config.html}{Validator Configuration File Format specification}~\cite{validatorConf}.

RIB manager supports two modes of prefix registration: \textbf{localhost} and \textbf{localhop}.
In \textbf{localhop} mode, RIB manager expects prefix registration requests from applications running on remote manchines, (i.e., NFD is running on an access router).
When localhop mode is enabled, {\tt rib} command Interests are accepted if the signing key can be authenticated along the naming hierarchy back to a (configurable) trust anchor.
For example, the trust anchor could be the root key of the NDN testbed, so that any user in the testbed can register prefixes through the RIB manager.
Alternatively, the trust anchor could be the key of a testbed site or instituion, thus limiting RIB manager's prefix registration to users at that site/institution.

In localhost mode, RIB manager expects to receive prefix registration requests from local applications.
By default, RIB manager allows any local application to register prefixes
However, the NFD administrator may also define their own access control rules using the same configuration format as the trust model configuration for localhop mode.

\subsection{Local Key Management}

NFD runs as a user level application. 
Therefore, NFD will try to access the keys owned by the user who runs NFD.
The information about a user's key storage can be found in a configuration file {\tt client.conf}.
NFD will search the configuration file at three places (in order): user home directory ({\tt \textasciitilde /.ndn/client.conf}), {\tt /usr/local/etc/ndn/client.conf}, and {\tt /etc/ndn/client.conf}.
Configuration file specify the locator of two modules: Public-key Information Base (PIB) and Trusted Platform Module (TPM). 
The two modules are paired up.
TPM is a secure storage for private keys, while PIB is provide public information about signing keys in the corresponding TPM.
NFD will lookup available keys in the database pointed by PIB locator, and send packet signing request to the TPM pointed by TPM locator.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
