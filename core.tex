\section{Common Services}
\label{sec:core}

NFD contains several common services to support forwarding and management operations.
These services are an essential part of the source code, but are logically separated and placed into the \texttt{core/} folder.

In addition to core services, NFD also relies extensively on libndn-cxx support, which provides many basic functions such as: packet format encoding/decoding, data structures for management protocol, and security framework.
The latter, within the context of NFD, is described in more detail in Section~\ref{sec:security}.

\subsection{Configuration File}\label{sec:core-config}

Many aspects of NFD are configurable through a configuration file, which adopts the Boost INFO format~\cite{boost-property-tree}.
This format is very flexible and allows any combination of nested configuration structures.

\subsubsection{User Info}

Currently, NFD defines 6 top level configuration sections: \emph{general}, \emph{tables}, \emph{log}, \emph{face\_system}, \emph{security}, and \emph{rib}.

\begin{itemize}

\item \textbf{general}:
The general section defines various parameters affecting the overall behavior of NFD.
Currently, the implementation only allows \texttt{user} and \texttt{group} parameter settings.
These parameters define the effective user and effective group that NFD will run as.
Note that using an effective user and/or group is different from just dropping privileges.
Namely, it allows NFD to regain superuser privileges at any time.
By default, NFD must be initially run with and be allowed to regain superuser privileges in orde to
access raw ethernet interfaces (Ethernet face support) and create a socket file in the system folder (Unix face support).
Temporarily dropping privileges by setting the effective user and group id provides minimal security risk mitigation, but it can also prevent well intentioned, but buggy, code from harming the underlying system.
It is also possible to run NFD without superuser privileges, but it requires the disabling of ethernet faces (or proper configuration to allow non-root users to perform privileged operations on sockets) and modification of the Unix socket path for NFD and all applications (see your installed nfd.conf configuration file or nfd.conf.sample for more details).
When applications are built using the ndn-cxx library, the Unix socket path for the application can be changed using the \verb|client.conf| file. The library will search for client.conf in three specific locations and in the following order:

\begin{itemize}
\item \verb|~/.ndn/client.conf|
\item \verb|/SYSCONFDIR/ndn/client.conf| (by default, SYSCONFDIR is \verb|/usr/local/etc|)
\item \verb|/etc/ndn/client.conf|
\end{itemize}

\item \textbf{tables}:
The tables section configures NFD's tables: Content Store, PIT, FIB, Strategy Choice, Measurements, and Network Region.
NFD currently supports configuring the maximum Content Store size, per-prefix strategy choices, and network region names:

\begin{itemize}
\item \verb|cs_max_packets|: Content Store size limit in number of packets.  Default is 65536, which corresponds to about 500~MB, assuming maximum size if 8~KB per Data packet.
\item \verb|strategy_choice|: This subsection selects the initial forwarding strategy for each specified prefix. Entries are listed as \verb|<namespace> <strategy-name>| pairs.
\item \verb|network_region|: This subsection contains a set of network regions used by the forwarder to determine if an Interest carrying a Link object has reached the producer region. Entries are a list of \verb|<names>|.
\end{itemize}

\item \textbf{log}:
The log section defines the logger configuration such as the default log level and individual NFD component log level overrides.  The log section is described in more detail in the Section~\ref{sec:basic-logger}.

\item \textbf{face\_system}:
The face system section fully controls allowed face protocols, channels and channel creation parameters, and enabling multicast faces.  Specific protocols may be disabled by commenting out or removing the corresponding nested block in its entirety. Empty sections will result in enabling the corresponding protocol with its default parameters.

NFD supports the following face protocols:

\begin{itemize}
\item \textbf{unix}: Unix protocol

This section can contain the following parameter:
  \begin{itemize}
  \item \verb|path|: sets the path for Unix socket (default is \verb|/var/run/nfd.sock|)
  \end{itemize}

Note that if the \textbf{unix} section is present, the created Unix channel will always be in a ``listening'' state. Commenting out the \textbf{unix} section disables Unix channel creation.

\item \textbf{udp}: UDP protocol

This section can contain the following parameters:
  \begin{itemize}
  \item \verb|port|: sets UDP unicast port number (default is 6363)
  \item \verb|enable_v4|: controls whether IPv4 UDP channels are enabled (enabled by default)
  \item \verb|enable_v6|: controls whether IPv6 UDP channels are enabled (enabled by default)
  \item \verb|idle_timeout|: sets the idle time in seconds before closing a UDP unicast face (default is 600 seconds)
  \item \verb|keep_alive_timeout|: sets the interval (seconds) between keep-alive refreshes  (default is 25 seconds)
  \item \verb|mcast|: controls whether UDP multicast faces need to be created (enabled by default)
  \item \verb|mcast_port|: sets UDP multicast port number (default is 56363)
  \item \verb|mcast_group|: UDP IPv4 multicast group (default is 224.0.23.170)
  \end{itemize}

Note that if the \textbf{udp} section is present, the created UDP channel will always be in a ``listening'' state as UDP is a session-less protocol and ``listening'' is necessary for all types of face operations.

\item \textbf{tcp}: TCP protocol

This section can contain the following parameters:
  \begin{itemize}
  \item \verb|listen|: controls whether the created TCP channel is in listening mode and creates TCP faces when an incoming connection is received (enabled by default)
  \item \verb|port|: sets the TCP listener port number (default is 6363)
  \item \verb|enable_v4|: controls whether IPv4 TCP channels are enabled (enabled by default)
  \item \verb|enable_v6|: controls whether IPv6 TCP channels are enabled (enabled by default)
  \end{itemize}

\item \textbf{ether}: Ethernet protocol (NDN directly on top of Ethernet, without requiring IP protocol)

This section can contain the following parameters:
  \begin{itemize}
  \item \verb|mcast|: controls whether Ethernet multicast faces need to be created (enabled by default)
  \item \verb|mcast_group|: sets the Ethernet multicast group (default is \texttt{01:00:5E:00:17:AA})
  \end{itemize}

Note that the Ethernet protocol only supports multicast mode at this time.
Unicast mode will be implemented in future versions of NFD.

\item \textbf{websocket}: The WebSocket protocol (tunnels to connect from JavaScript applications running in a web browser)

This section can contain the following parameters:
  \begin{itemize}
  \item \verb|listen|: controls whether the created WebSocket channel is in listening mode and creates WebSocket faces when incoming connections are received (enabled by default)
  \item \verb|port| 9696 ; WebSocket listener port number
  \item \verb|enable_v4|: controls whether IPv4 WebSocket channels are enabled (enabled by default)
  \item \verb|enable_v6|: controls whether IPv6 WebSocket channels are enabled (enabled by default)
  \end{itemize}

\end{itemize}

\item \textbf{authorizations}:
The \verb|authorizations| section provides a fine-grained control for management operations.  As described in Section~\ref{sec:mgmt}, NFD has several managers, the use of which can be authorized to specific NDN users.
For example, the creation and destruction of faces can be authorized to one user, management of FIB to another, and control over strategy choice to a third user.

To simplify the initial bootstrapping of NFD, the sample configuration file does not restrict local NFD management operations: any user can send management commands to NFD and NFD will authorize them.  However, such configuration should not be used in a production environment and only designated users should be authorized to perform specific management operations.

The basic syntax for the \verb|authorizations| section is as follows.  It consists of zero or more \verb|authorize| blocks. Each \verb|authorize| block associates a single NDN identity certificate, specified by the \verb|certfile| parameter, with \verb|privileges| blocks. The \verb|privileges| block
defines a list of permissions/managers (one permission per line) that are granted to the user identified by
\verb|certfile| defines a file name (relative to the configuration file format) of the NDN certificate.
As a special case, primarily for demo purposes, certfile accepts value "any", which denotes any certificate possessed by any user.
Note that all managers controlled by the \verb|authorizations| section are local.  In other words, all commands start with \verb|/localhost|, which are possible only through local faces (Unix face and TCP face to 127.0.0.1).

\textbf{Note for developers:}

The \verb|privileges| block can be extended to support additional permissions with the creation of new managers (see Section~\ref{sec:mgmt}). This is achieved by deriving the new manager from the \verb|ManagerBase| class. The second argument to the \verb|ManagerBase| constructor specifies the desired permission name.

\item \textbf{rib}:
The \verb|rib| section controls behavior and security parameters for NFD RIB manager.
This section can contain three subsections: \verb|localhost_security|, \verb|localhop_security|, and \verb|auto_prefix_propagate|.
\verb|localhost_security| controls authorizations for registering and unregistering prefixes in RIB from local users (through local faces: Unix socket or TCP tunnel to 127.0.0.1).
\verb|localhop_security| defines authorization rules for so called localhop prefix registrations: registration of prefixes on the next hop routers.
\verb|auto_prefix_propagate| configures the behavior of the Auto Prefix Propagator feature of the RIB manager (Section~\ref{sec:rib-propagation}).

Unlike the main \verb|authorizations| section, the rib security section uses a more advanced validator configuration, thus allowing a greater level of flexibility in specifying authorizations.
In particular, it is possible to specify not only specific authorized certificates, but also indirectly authorized certificates.
For more details about validator configuration and its capabilities, refer to Section~\ref{sec:security} and \href{http://named-data.net/doc/ndn-cxx/current/tutorials/security-validator-config.html}{Validator Configuration File Format specification}~\cite{validatorConf}.

Similar to the \verb|authorizations| section, the sample configuration file, allows any local user to send register and unregister commands (\verb|localhost_security|) and prohibits remote users from sending registration commands (the \verb|localhop_security| section is disabled).
On NDN Testbed hubs, the latter is configured in a way to authorize any valid NDN Testbed user (i.e., a user possessing valid NDN certificate obtained through \href{https://github.com/named-data/ndncert}{ndncert website}~\cite{ndncert}) to send registration requests for user namespace.
For example, a user Alice with a valid certificate \texttt{/ndn\allowbreak /site\allowbreak /alice\allowbreak /KEY\allowbreak /...\allowbreak /ID-CERT/...} would be allowed to register any prefixes started with \verb|/ndn/site/alice| on NDN hub.

The \verb|auto_prefix_propagate| subsection supports configuring the forwarding cost of remotely registered prefixes, the timeout interval of a remote prefix registration command, how often propagations are refreshed, and the minimum and maximum wait time before retrying a propagation:

\begin{itemize}
\item \verb|cost|: The forwarding cost for prefixes registered on a remote router (default is 15).
\item \verb|timeout|: The timeout (in milliseconds) of prefix registration commands for propagation (default is 10000).
\item \verb|refresh_interval|: The interval (in seconds) before refreshing the propagation (default is 300). This setting should be less than \verb|face_system.udp.idle_time|, so that the face is kept alive on the remote router.
\item \verb|base_retry_wait|: The base wait time (in seconds) before retrying propagation (default is 50).
\item \verb|max_retry_wait|: maximum wait time (in seconds) before retrying propagation between consecutive retries (default is 3600). The wait time before each retry is calculated based on the following back-off policy: initially, the wait time is set to \verb|base_retry_wait|. The wait time is then doubled for each retry unless it is greater than \verb|max_retry_wait|, in which case the wait time is set to \verb|max_retry_wait|.
\end{itemize}

\subsubsection{Developer Info}

When creating a new management module, it is very easy to make use of the NFD configuration file framework.
Most heavy lifting is performed using the Boost.PropertyTree~\cite{boost-property-tree} library and NFD implements an additional wrapper (ConfigFile) to simplify configuration file operations.

\begin{enumerate}

\item Define the format of the new configuration section.
Reusing an existing configuration section could be problematic, since a diagnostic error will be generated any time an unknown parameter is encountered.

\item The new module should define a callback with prototype \texttt{void(*)(ConfigSection..., bool isDryRun)} that implements the actual processing of the newly defined section.
The best guidance for this step is to take a look at the existing source code of one of the managers and implement the processing in a similar manner.
The callback can support two modes: dry-run to check validity of the specified parameters, and actual run to apply the specified parameters.

As a general guideline, the callback should be able to process the same section multiple times in actual run mode without causing problems.
This feature is necessary in order to provide functionality of reloading configuration file during run-time.  In some cases, this requirement may result in cleaning up data structures created during the run.
If it is hard or impossible to support configuration file reloading, the callback must detect the reloading event and stop processing it.

\item Update NFD initialization in \verb|daemon/nfd.hpp| and \verb|daemon/nfd.cpp| files.  In particular, an instance of the new management module needs to be created inside the \texttt{initializeManagement} method.  Once module is created, it should be added to ConfigFile class dispatch.  Similar updates should be made to \verb|reloadConfigFile| method.

As another general recommendation, do not forget to create proper test cases to check correctness of the new config section processing.  This is vital for providing longevity support for the implemented module, as it ensures that parsing follows the specification, even after NFD or the supporting libraries are changed.

\end{enumerate}
\end{itemize}

\subsubsection{Configuration Reload}\label{sec:core-config-reload}

NFD reloads the configuration upon \textbf{SIGHUP} signal.

\subsection{Basic Logger}
\label{sec:basic-logger}
One of the most important core services is the logger.
NFD's logger provides support for multiple log levels, which can be configured in the configuration file individually for each module.
The configuration file also includes a setting for the default log level that applies to all modules, except explicitly listed.

\subsubsection{User Info}

Log level is configured in the \verb|log| section of the configure file.
The format for each configuration setting is a key-value pair, where key is name of the specific module and value is the desired log level.
Valid values for log level are:

\begin{itemize}
\item \textbf{NONE}: no messages
\item \textbf{ERROR}: show only error messages
\item \textbf{WARN}: show also warning messages
\item \textbf{INFO}: show also informational messages (default)
\item \textbf{DEBUG}: show also debugging messages
\item \textbf{TRACE}: show also trace messages
\item \textbf{ALL}: all messages for all log levels (most verbose)
\end{itemize}

Individual module names can be found in the source code by looking for \verb|NFD_LOG_INIT(<module name>)| statements in \verb|.cpp| files, or using \verb|--modules| command-line option for the \verb|nfd| program.
There is also a special \verb|default_level| key, which defines log level for all modules, except explicitly specified (if not specified, \verb|INFO| log level is used).

\subsubsection{Developer Info}

To enable NFD logging in a new module, very few actions are required from the developer:

\begin{itemize}
\item include \verb|core/logger.hpp| header file
\item declare logging module using \verb|NFD_LOG_INIT(<module name>)| macros
\item use \verb|NFD_LOG_<LEVEL>(statement to log)| in the source code
\end{itemize}

The effective log level for unit testing is defined in \verb|unit-tests.conf| (see sample \verb|unit-tests.conf.sample| file) rather the normal \verb|nfd.conf|. \verb|unit-tests.conf| is expected under the top level NFD directory (i.e. same directory as the sample file).

\subsection{Hash Computation Routines}

Common services also include several hash functions, based on city hash algorithm~\cite{cityhash}, to support fast name-based operations.
Since efficient hash table index size depends on the platform, NFD includes several versions, for 16-bit, 32-bit, 64-bit, and 128-bit hashing.%
\footnote{Even though the performance is not a primary goal for the current implementation, we tried to be as much efficient as possible within the developed framework.}

Name tree implementation generalizes the platform-dependent use of hash functions using a template-based helper (see \verb|computeHash| function in \texttt{daemon/tables/name-tree.cpp}).
Depending on the size of \verb|size_t| type on the platform, the compiler will automatically select the correct version of the hash function.

Other hash functions may be included in the future to provide tailored implementations for specific usage patterns.
In other words, since the quality of the hash function is usually not the sole property of the algorithm, but also relies on the hashed source (hash functions need to hash uniformly into the hash space), depending on which Interest and Data names are used, other hash functions may be more appropriate.
Cryptographic hash functions are also an option, however they are usually prohibitively expensive.

\subsection{Global Scheduler}\label{sec:core-scheduler}

The ndn-cxx library includes a scheduler class that provides a simple way to schedule arbitrary events (callbacks) at arbitrary time points.  Normally, each module/class creates its own scheduler object.
An implication of this is that a scheduled object, when necessary, must be cancelled in a specific scheduler, otherwise the behavior is undefined.

NFD packet forwarding has a number of events with shared ownership of events.
To simplify this and other event operations, common services include a global scheduler.
To use this scheduler, one needs to include \verb|core/scheduler.hpp|, after which new events can be scheduled using the \verb|scheduler::schedule| free function.
The scheduled event can then be cancelled at any time by calling the \verb|scheduler::cancel| function with the event id that was originally returned by \verb|scheduler::schedule|.

\subsection{Global IO Service}

The NFD packet forwarding implementation is based on Boost.Asio~\cite{boost-asio}, which provides efficient asynchronous operations.
The main feature of this is the \verb|io_service| abstraction.
\verb|io_service| implements the dispatch of any scheduled events in an asynchronous manner, such as sending packets through Berkeley sockets, processing received packets and connections, and many others including arbitrary function calls (e.g., scheduler class in ndn-cxx library is fully based on \verb|io_service|).

Logically, \verb|io_service| is just a queue of callbacks (explicitly or implicitly added).
 In order to actually execute any of these callback functions, at least one processing thread should be created.
This is accomplished by calling the \verb|io_service::run| method.
The execution thread that called the run method then becomes such an execution thread and starts processing enqueued callbacks in an application-defined manner.
Note that any exceptions that will be thrown inside the enqueued callbacks can be intercepted in the processing thread that called the run method on \verb|io_service| object.

The current implementation of NFD uses a single global instance of \verb|io_service| object with a single processing thread.
This thread is initiated from the main function (i.e., main function calls \verb|run| method on the global \verb|io_service| instance).

In some implementations of new NFD services, it may be required to specify a \verb|io_service| object.
For example, when implementing TCP face, it is necessary to provide an \verb|io_service| object as a constructor parameter to \texttt{boost::\allowbreak asio::\allowbreak ip::\allowbreak tcp::\allowbreak socket}.
In such cases, it is enough to include \texttt{core/global-io.hpp} header file and supply \verb|getGlobalIoService()| as the argument.
The remainder will be handled by the existing NFD framework.

\subsection{Privilege Helper}

When NFD is run as a super user (may be necessary to support Ethernet faces, enabling TCP/UDP/WebSocket faces on privileged ports, or enabling Unix socket face in a root-only-writeable location), it is possible to run most of the operations in unprivileged mode.
In order to do so, NFD includes a PrivilegeHelper that can be configured through configuration file to drop privileges as soon as NFD initialization finishes.
When necessary, NFD can temporarily regain privileges to do additional tasks, e.g., (re-)create multicast faces.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
