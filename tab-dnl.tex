\subsection{Dead Nonce List}\label{sec:tab-dnl}

The Dead Nonce List is a data structure that supplements the PIT for loop detection purposes.

In August 2014, we found a persistent loop problem when InterestLifetime is short (Bug 1953).
Loop detection previously used only the Nonces stored in PIT entries.
If an Interest is not satisfied within InterestLifetime, the PIT entry is deleted.
When the network contains a cycle whose delay is longer than InterestLifetime, a looping Interest around this cycle cannot be detected because the PIT entry is gone before the Interest loops back.

A naive solution to this persistent loop problem is to keep the PIT entry for a longer duration.
However, the memory consumption of doing so would be too high, because a PIT entry contains many other things than the Nonce.
Therefore, the Dead Nonce List is introduced to store Nonces ``dead'' from the PIT.

The Dead Nonce List is a global container in NFD.
Each entry in this container stores a tuple of Name and Nonce.
The existence of an entry can be queried efficiently.
Entries are kept for a duration after which the Interest is unlikely to loop back.

The structure and semantics of the Dead Nonce List, and how it's used by forwarding are described in Section~\ref{sec:tab-dnl-sem}.
Section~\ref{sec:tab-dnl-capacity} discusses how the capacity of Dead Nonce List is maintained.


\subsubsection{Structure, Semantics, and Usage}\label{sec:tab-dnl-sem}

A tuple of Name and Nonce is added to Dead Nonce List (\texttt{DeadNonceList::add}) in \textit{incoming Data pipeline} (Section~\ref{sec:fw-incoming-data}) and \textit{Interest finalize pipeline} (Section~\ref{sec:fw-interest-finalize}) before out-records are deleted.

The Dead Nonce List is queried (\texttt{DeadNonceList::has}) in \textit{incoming Interest pipeline} (Section~\ref{sec:fw-incoming-interest}).
If an entry with same Name and Nonce exists, the incoming Interest is a looping Interest.

The Dead Nonce List is a probabilistic data structure: each entry is stored as a 64-bit hash of the Name and Nonce.
This greatly reduces the memory consumption of the data structure.
At the same time, there's a non-zero probability of hash collisions, which inevitably cause false positives: non-looping Interests are mistaken as looping Interests.
Those false positives are recoverable: the consumer can retransmit the Interest with a fresh Nonce, which most likely would yield a different hash that doesn't collide with an existing one.
We believe the gain from memory savings outweighs the harm of false positives.


\subsubsection{Capacity Maintenance}\label{sec:tab-dnl-capacity}

Entries are kept in the Dead Nonce List for a configurable lifetime.
The lifetime of an entry is a trade-off between the effectiveness of loop detection, the memory consumption of the container, and the probability of false positives.
A longer lifetime improves the effectiveness of loop detection, because a looping Interest can be detected only if it loops back before the entry is removed, therefore the longer lifetime allows detecting looping Interests in network cycles with a longer delay.
On the other hand, a longer entry lifetime causes more entries to be stored, and therefore increases the memory consumption of the container; keeping more entries also means a higher probability of hash collisions and thus false positives.
The default entry lifetime is set to 6 seconds.

A naive approach to entry lifetime enforcement is to keep a timestamp in each entry.
This approach consumes too much memory.
Given that the Dead Nonce List is a probabilistic data structure, entry lifetime doesn't need to be precise.
Thus, we index the container as a first-in-first-out queue, and we approximate the entry lifetime to the configured lifetime by adjusting the capacity of the container.

It's infeasible to statically configure the capacity of the container, because the frequency of adding entries is correlated to Interest arrival rate, which cannot be accurately estimated by an operator.
Therefore, we use the following algorithm to dynamically adjust the capacity for \textit{expected} entry lifetime $L$:
\begin{itemize}
\item At interval $M$, we add a special entry called \textit{mark} to the container.
      The mark doesn't have a distinct type: it's an entry with a specific value, with the assumption that the hash function is non-invertible so that the probability of colliding with a hash value computed from Name and Nonce is low.
\item At interval $M$, we count the number of marks in the container, and remember the count.
      The order between adding a mark and counting marks doesn't matter, but it shall be consistent.
\item At interval $A$, we look at recent counts.
      When the capacity of the container is optimal, there should be $L/M$ marks in the container at all times.
      If all recent counts are above $L/M$, the capacity is decreased.
      If all recent counts are below $L/M$, the capacity is increased.
\end{itemize}

In addition, there is a hard upper and lower bound for the capacity, to avoid memory overflow and to ensure correct operations.
When the capacity is adjusted down, to bound algorithm execution time, excess entries are not evicted all at once, but are evicted in batches during future insertion operations.
