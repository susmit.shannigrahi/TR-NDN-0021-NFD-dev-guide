\subsubsection{Access Router Strategy}\label{sec:strategy-access}

The access router strategy (aka access strategy) is specifically designed for local site prefix on an access/edge router.
It is suitable for a namespace where producers are single-homed and are one hop away.
This strategy is implemented as \texttt{nfd::fw::AccessStrategy} class.

The strategy is able to make use of multiple paths in the FIB entry, and remember which path can lead to contents.
It is most efficient when FIB nexthops are accurate, but can tolerate imprecise nexthops, and still be able to find the correct paths.

The strategy is able to recover from a packet loss in the last-hop link.
It retries Interests retransmitted by consumer in the same manner as best route strategy (Section~\ref{sec:strategy-best-route});
The same mechanism also allows the strategy to deal with producer mobility.

\paragraph{Motivation and Use Case}

One of NDN's benefits is that it does not require precise routing: a route indicates that contents under a certain prefix is \textit{probably} available from a nexthop.
The property brings a challenge to forwarding strategy design: if an Interest matches multiple routes, which nexthop should we forward it to?
\begin{asparaitem}
\item One option is to forward the Interest to all the nexthops.
      This is implemented in the multicast strategy (Section~\ref{sec:strategy-multicast}).
      The Data, if available from any of these nexthops, can be retrieved with shortest delay.
      However, since every Interest is forwarded to many nexthops, it has significant bandwidth overhead.
\item Another option is to forward the Interest to only one nexthop, and if it doesn't work, try another nexthop.
      This is implemented in the best route strategy (Section~\ref{sec:strategy-best-route}).
      While bandwidth overhead on the upstream side is reduced, since each incoming Interest can be forwarded to only one nexthop, the consumer has to retransmit an Interest multiple times in order to reach the correct nexthop.
      It's even worse when an upstream does not return a Nack or the Nack is lost; in this case, the consumer has to wait for a timeout (either based on \textit{InterestLifetime} or RTO) before it can decide to retransmit, causing further delays.
      In addition, repeated consumer retransmissions increase bandwidth overhead on the downstream side.
\item Between these two extremes, we want to design a strategy with a trade-off between delay and bandwidth overhead.
\end{asparaitem}

On gateway/access/edge routers of the NDN testbed, despite the availability of \textit{automatic prefix propagation} (Section~\ref{sec:rib-propagation}), the majority of Interest forwarding from an access router to end hosts are relying on routes installed by the \texttt{nfd-autoreg} tool: when an end host connects to the router, this tool installs a route for the \textit{local site prefix} toward this end host.
Since the local site prefix is statically configured, these routes will have the same prefix, and an Interest under this prefix will match all these routes.
This is an extreme case of imprecise routing: every end host is a nexthop of a very broad prefix, and they are many end hosts.
The multicast strategy would forward every Interest toward all end hosts, even if only a small subset of them can serve the content.
The best route strategy would require the consumer to retransmit, in the worst case, as many times as the number of connected end hosts.

The access strategy is designed for this use case.
We want to find contents available on end hosts without forwarding every Interest to all end hosts, and require minimal consumer retransmissions.

\paragraph{How Access Router Strategy Works}

The basic idea of this strategy is to multicast the first Interest, learn which nexthop can serve the contents, and forward the bulk of subsequent Interests toward the chosen nexthop; if it does not respond, the strategy starts multicasting again to find an alternate path.

This idea is somewhat similar to Ethernet self-learning, but there are two challenges:
\begin{asparaitem}
\item Ethernet switch learns the mapping from an \textbf{exact} address to a switch port, but NDN router needs to learn the mapping from a \textbf{prefix} of the Interest name to a nexthop in order to be useful for subsequent Interests.
      What prefix can we learn from Interest-Data exchanges?
\item In Ethernet, each address is reachable via only one path, and a moved host floods an ARP packet to inform the network about its new location.
      In NDN, each prefix can be reachable via multiple paths, and it's the strategy's responsibility to detect the chosen nexthop is no longer working so it can start finding an alternate path; a moved producer won't actively inform the network of its new location, and a failed producer has no way to ask other producers to flood an announcement.
\end{asparaitem}

\paragraph{Granularity Problem and Solution}

The first challenge, what prefix can we learn from Interest-Data exchanges, is called the \textbf{granularity problem}.
There are a few differnet approaches to solve this problem, but we pick a simple solution in the access strategy: learned nexthop is associated with the Data name minus the last component.
A commonly adopted NDN naming convention \cite{NamingConventions} puts the version number and segment number as the last two components of Data names.
Under this naming convention, the Data name minus the last component covers all segments of a versioned object.
We believe it's safe to assume that all segments of a version is available on the same upstream, and thus choose this solution.

As a consequence of choosing this simple solution, the access strategy could perform badly if the application does not follow the above naming convention.
Most notably, NDN-RTC \cite{NdnRtc} realtime conference library (version 1.3 when we did this analysis in Sep 2015 \footnote{More details of this analysis can be found at \url{http://redmine.named-data.net/issues/3219}.}) adopts a naming scheme which expresses Interests similar to \path{/ndn/edu/ucla/remap/ndnrtc/user/remap/streams/camera\_1469c/mid/key/2991/data/\%00\%00} and generates Data names similar to \path{/ndn/edu/ucla/remap/ndnrtc/user/remap/streams/camera\_1469c/mid/key/2991/data/\%00\%00/23/89730/86739/5/27576}.
In this name, the component before \texttt{data} is the frame number (\texttt{2991} in the example), and the component after \texttt{data} is the segment number (\texttt{\%00\%00} in the example); every Data name has 5 additional components than the Interest name, which carries additional signaling information for application use.

Admittedly, this is an example of bad naming design because although NDN supports in-network name discovery, the majority of Interests should carry complete names \cite{DesignPrinciples}; appending application-layer metadata onto the Data name violates this principle.
This naming design makes the access strategy multicast every Interest, because the chosen nexthop of an Interest is recorded on the Data name minus the last component (\path{/ndn/edu/ucla/remap/ndnrtc/user/remap/streams/camera\_1469c/mid/key/2991/data/\%00\%00/23/89730/86739/5}), but the next Interest is \path{/ndn/edu/ucla/remap/ndnrtc/user/remap/streams/camera\_1469c/mid/key/2991/data/\%00\%01} which does not fall under the prefix where the chosen nexthop is recorded.
As a result, the next Interest is still treated as an ``initial Interest'' and multicast.

However, even if we change NDN-RTC's naming scheme so that the Data name is same as the Interest name (i.e. ends with the segment number), AccessStrategy would only perform slightly better.
The chosen nexthop would be recorded on \path{/ndn/edu/ucla/remap/ndnrtc/user/remap/streams/camera\_1469c/mid/key/2991/data}, which matches subsequent Interests for other segments within the same frame, but cannot match Interests for other frames (such as \path{/ndn/edu/ucla/remap/ndnrtc/user/remap/streams/camera\_1469c/mid/key/2992/data/\%00\%00}).
Within a 700Kbps video stream, the frame number changes more than 50 times per second, and there are about 25 segment numbers per frame.
This means, the access strategy would multicast about 1250 times per second with NDN-RTC 1.3's naming scheme; each of those multicast Interests would reach every end host that has a nexthop added by \texttt{nfd-autoreg}, increase their bandwidth usage and CPU overhead.
Changing the Data name to be same as the Interest name would reduce multicasts to 50 times per second, which is still inefficient.

The fundamental reason of access strategy's inefficiency with NDN-RTC 1.3 is the mismatch between the assumption of naming convention behind our granularity solution and the application naming scheme: the chosen nexthop is recorded at a longer prefix than the actual prefix.
It's also possible for the chosen nexthop to be recorded at a too-short prefix, but no known application can suffer from the problem; however, if we change the access strategy to record the chosen nexthop at shorter prefixes (such as dropping last 3 components of the Data name, which would accommodate NDN-RTC's naming scheme after modification), this problem would happen.

It's our future work to explore other solutions to the granularity problem.

\paragraph{Failure Detection}

The second challenge, how to detect the chosen nexthop is no longer working, is currently solved with a combination of RTT-based timeout and consumer retransmission.

\begin{description}
\item[RTT-based timeout]
We maintain RTT estimations following TCP's algorithm.
If a nexthop does not return Data within the Retransmission Timeout (RTO) computed from the RTT estimator, we consider the chosen nexthop to have failed, and multicast the Interest toward all nexthops (except the chosen nexthop, because otherwise the upstream would see a duplicate Nonce).

There are two kinds of RTT estimators: per-prefix RTT estimators, and per-face RTT estimators.
On each prefix where we learn a chosen nexthop (i.e. the Data name minus the last component), we maintain a per-prefix RTT estimator; if a subsequent Interest is not satisified within the RTO computed from this per-prefix RTT estimator, the strategy would multicast the Interest.
A per-prefix RTT estimator reflects the RTT of the current chosen nexthop; if a different nexthop is chosen, this RTT estimator shall be reset.

In order to have a good enough initial estimation, when we reset a per-prefix RTT estimator, instead of starting with a set of default values, we copy the state from the per-face RTT estimator, which is maintained for each upstream face and not associated with name prefixes.
This design assumes different prefixes served by the same upstream face have similar RTT; this assumption is one reason that this strategy design is limited for use on one hop, and is unfit for usage over multiple hops.

\item[consumer retransmission]
Some consumer applications have application-layer means to detect a non-working path.
If the consumer believes the current path is not working, it could retransmit the Interest with a new Nonce.
Unless the retransmissions are too frequent, the access strategy would take an retransmission as a signal that the chosen nexthop has stopped working, and multicast the Interest right away.

\end{description}
