\section{Testing}\label{sec:test}

In general, software testing consists of multiple testing levels.
The levels that have been majorly supported during the NFD development process include unit and integration tests.

At the \emph{unit test level}, individual units of source code (usually defined in a single \verb|.cpp| file) are tested for functional correctness.
Some of the developed unit tests in NFD involve multiple modules and interaction between modules (e.g., testing of forwarding strategies).
However, even in these cases, all testing is performed internally to the module, without any external interaction.

NFD also employs \emph{integration testing}, where NFD software and its components is evaluated as a whole in a controlled networking environment.

\subsection{Unit Tests}\label{sec:test-unit}

NFD uses Boost Test Library~\cite{boost-test} to support development and execution of unit testing.

\subsubsection{Test Structure}\label{sec:test-unit-struct}

Each unit test consists of one or more test suites, which may contain one or more locally defined classes and/or attributes and a number of test cases.
A specific test case should test a number of the functionalities implemented by a NFD module.
In order to reuse common functions among the test suites of a test file or even among multiple test files, one can make use of the fixture model provided by the Boost Test Library.

For example, the \verb|tests/daemon/face/face.t.cpp| file, which tests the correctness of the \verb|daemon/face/face.hpp| file, contains a single test suite with a number of test cases and a class that extends the DummyFace class.
The test cases check the correctness of the functionalities implemented by the \verb|daemon/face/face.hpp| file.

\subsubsection{Running Tests}\label{sec:test-unit-run}

In order to run the unit tests, one has to configure and compile NFD with the \verb|--with-tests| parameter.
Once the compilation is done, one can run all the unit tests by typing:

\begin{verbatim}
    ./build/unit-tests
\end{verbatim}

One can run a specific test case of a specific test suite by typing:

\begin{verbatim}
    ./build/unit-tests -t <TestSuite>/<TestCase>
\end{verbatim}

\subsubsection{Test Helpers}\label{sec:test-unit-helper}

ndn-cxx library provides a number of helper tools to facilitate development of unit tests:

\begin{itemize}
\item \textbf{DummyClientFace} {\texttt{<ndn-cxx/util/dummy-client-face.hpp>}}:
  a socket-independent Face abstraction to be used during the unit testing process;

\item \textbf{UnitTestSystemClock} and \textbf{UnitTestSteadyClock} (\texttt{<util/time-unit-test-clock.hpp>}):
  abstractions to mock system and steady clocks used inside ndn-cxx and NFD implementation.

\end{itemize}


In addition to library tools, NFD unit test environment also includes a few NFD-specific common testing elements:

\begin{itemize}

\item \textbf{LimitedIo} (\texttt{tests/limited-io.hpp}):
  class to start/stop IO operations, including operation count and/or time limit for unit testing;

\item \textbf{UnitTestTimeFixture} (\texttt{tests/test-common.hpp}):
  a base test fixture that overrides steady clock and system clock;

\item \textbf{IdentityManagementFixture} (\texttt{tests/identity-management-fixture.hpp}):
  a test suite level fixture that can be used fixture to create temporary identities.
  Identities added via \texttt{IdentityManagementFixture::addIdentity} method are automatically deleted during test teardown.

\item \textbf{DummyTransport} (\texttt{tests/daemon/face/dummy-transport.hpp}):
  a dummy Transport used in testing Faces and LinkServices

\item \textbf{DummyReceiveLinkService} (\texttt{tests/daemon/face/dummy-receive-link-service.hpp}):
  a dummy LinkService that logs all received packets. Note that this LinkService does not allow sending of packets.

\item \textbf{StrategyTester} (\texttt{tests/daemon/fw/strategy-tester.hpp}):
  a framework to test a forwarding strategy.
  This helper extends the tested strategy to offer recording of its invoked actions, without passing them to the actual forwarder implementation.

\item \textbf{TopologyTester} (\texttt{tests/daemon/fw/topology-tester.hpp}):
  a framework to construct a virtual mock topology and implement various network events (e.g., failing and recovering links).
  The purpose is to test the forwarder and an implemented forwarding strategy across this virtual topology.

\end{itemize}

\subsubsection{Test Code Guidelines and Naming Conventions}\label{sec:test-unit-guideline}

The current implementation of NFD unit tests uses the following naming convention:

\begin{itemize}

\item A test suite for the \verb|folder/module-name.hpp| file should be placed in \verb|tests/folder/module-name.t.cpp|.
  For example, the test suite for the \verb|daemon/fw/forwarder.hpp| file should be placed in \verb|tests/daemon/fw/forwarder.t.cpp|.

\item A test suite should be named as \texttt{TestModuleName} and nested under test suites named after directories.
  For example, the test suite for the \verb|daemon/fw/forwarder.hpp| file should be named \texttt{Fw/TestForwarder} (``daemon'' part is not needed, as daemon-related unit tests are separated into a separate unit tests module \texttt{unit-tests-daemon}).

\item Test suite should be declared inside the same namespace of the tested type plus additional \texttt{tests} namespace.
 For example, a test suite for the \texttt{nfd::Forwarder} class is declared in the namespace \texttt{nfd::tests} and a test suite for the \texttt{nfd::cs::Cs} class is declared in the \texttt{nfd::cs::tests} namespace.
  If needed, parent tests sub-namespace can be imported with using namespace directive.

\item A test suite should use the \texttt{nfd::tests::BaseFixture} fixture to get automatic setup and teardown of global \verb|io_service|.
  If a custom fixture is defined for a test suite or a test case, this custom fixture should derive from the \texttt{BaseFixture}.
  If a test case needs to sign data, it should use \texttt{IdentityManagementFixture} or fixture that extends it.
  When feasible, \texttt{UnitTestTimeFixture} should be used to mock clocks.

\end{itemize}

\subsection{Integration Tests}\label{sec:test-integ}

NFD team has developed a number of integrated test cases (\url{http://gerrit.named-data.net/#/admin/projects/NFD/integration-tests}) that can be run in a dedicated test environment.

The easiest way to run the integration tests is to use Vagrant (\url{https://www.vagrantup.com/}) to automate creation and running of VirtualBox virtual machines.  Note that the test machine will need at least 9Gb of RAM and at least 5 CPU threads.

\begin{itemize}
\item Install VirtualBox and Vagrant on the test system
\item Clone integration tests repository:
\begin{verbatim}
    git clone http://gerrit.named-data.net/NFD/integration-tests
    cd integration-tests
\end{verbatim}

\item Run \texttt{run-vagrant-tests.sh} script that will create necessary virtual machines and run all the integration tests.

\item Resulting logs can be collected using \texttt{collect-logs.sh} script.
\end{itemize}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "nfd-docs"
%%% End:
