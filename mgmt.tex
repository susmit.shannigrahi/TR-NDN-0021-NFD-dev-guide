\section{Management}\label{sec:mgmt}

NFD management offers the capability to monitor and control NFD through configuration file and Interest-based API.

NFD management is divided into several management modules.
Each management module is responsible for a subsystem of NFD.
It may initialize the subsystem from a section in the NFD configuration file, or offer an Interest-based API to allow others monitor and control the subsystem.

Section~\ref{sec:mgmt-overview} gives an overview on the NFD configuration file, and basic mechanisms in NFD Management protocol \cite{nfd-management}.
Section~\ref{sec:mgmt-dispatch} describes the dispatcher and authenticator used in NFD Management protocol implementation.
Section~\ref{sec:mgmt-status}, \ref{sec:mgmt-face}, \ref{sec:mgmt-fib}, \ref{sec:mgmt-sc} explain details of four major management modules.
Section~\ref{sec:mgmt-conf} introduces two additional management modules used in configuration file parsing.
Section~\ref{sec:mgmt-extend} offers ideas on how to extend NFD management.


\subsection{Protocol Overview}\label{sec:mgmt-overview}

All management actions that change NFD state require the use of \emph{control commands}~\cite{control-command}; a form of signed Interests.
These allow NFD to determine whether the issuer is authorized to perform the specified action.
Management modules respond with \emph{control responses} to inform the user of the commands success or failure.
Control responses have status codes similar to HTTP and describe the action that was performed or any errors that occurred.

Management actions that just query the current state of NFD do not need to be authenticated.
Therefore, these actions are defined in NFD Management Protocol~\cite{nfd-management} as \emph{status dataset}, and are currently implemented in NFD as a simple Interest/Data exchange.
In the future if data access control is desired, some data can be encrypted.


\subsection{Dispatcher and Authenticator}\label{sec:mgmt-dispatch}

Currently, as shown in figure~\ref{fig:mgmt:manager:dispatch}, all managers utilize the \emph{ndn::Dispatcher} as an agent to deal with high-level Interest / Data exchange, so that they can focus on low-layer operations toward the NFD.

\begin{figure}[htpb]
\centering
\includegraphics[width=.8\textwidth]{mgmt-manager-dispatch}
\caption{Overview of the manager Interest / Data exchange via the dispatcher.}
\label{fig:mgmt:manager:dispatch}
\end{figure}

More specifically, a manager always consists of a series of handlers, each of which is responsible to deal with a \emph{control command} request or a \emph{status dataset} request.
Per management protocol, those requests~\cite{nfd-management}, follow a namespace pattern of \path{/localhost/nfd/$<$manager-name$>$/<verb>}.
Here, \emph{verb} describes the action that the \emph{manager-name} manager should perform.
For example, \path{/localhost/nfd/fib/add-nexthop} directs the FIB Manager to add a next hop (command arguments follow the verb.

A handler is registered, by some manager, to the dispatcher with a partial name that is composed of the manger's name and a specified verb.
Then, after all managers have registered their handlers, the dispatcher will make full prefixes with those partial names and each registered top prefix (i.e., \texttt{/localhost\allowbreak /nfd}), and then sets interest filters for them.
When a request arrives the dispatcher, it will finally be directed to some management handler after processed by interest filters.
On the other hand, all types of respones, such as of \emph{control responses}, \emph{status datasets}, etc., will be back to the dispatcher, for concatenating, segmenting and singing when required.

For \emph{control command}, a handler is always registered along with an \emph{authorization} method and a \emph{parametersValidate} method.
Then, a request can be directed to the handler, if and only if it is accepted by the \emph{authorization}, and its \emph{control parameters} can be validated by the
\emph{parametersValidate}.
While for \emph{status dataset}, there is no need to validate parameters, and, currently, the \emph{authorization} is made to accept all requests\footnote{This may be changed whenever data access control is desired.}.

Besides, some managers (such as the FaceManger), that produces notifications, will also register \emph{notification streams} to the dispatcher.
And this type of registration returns a \emph{postNotification}, through which the manager will only need to genearte the notification content, leaving other parts of work (making packet, signing, etc.) to the dispatcher.

\subsubsection{Manager Base}\label{sec:mgmt-base}

\texttt{ManagerBase} is the base class for all managers.
This class holds the manager's shared \texttt{Dispatcher} and \texttt{CommandValidator}, and provides a number of commonly used methods.
In particular, \texttt{ManagerBase} provides the methods to register command handler, to register status dataset handler, and to register notification streams.
Besides, \texttt{ManagerBase} also provides convenience methods for authorizing \emph{control commands} (\texttt{authorize}), for validating \emph{control parameters} (\texttt{validateParameters}), and also for extracting the name of the requester from the Interest (\texttt{extractRequester}).

On construction, \texttt{ManagerBase} obtains a reference to a \texttt{Dispatcher} that is responsible to dispatch requests to the target management handler, and a
\texttt{CommandValidator} that will be used for control command authorization later.
Derived manager classes provide the \texttt{ManagerBase} constructor with the name of their \texttt{privilege} (e.g., \texttt{faces}, \texttt{fib}, or \texttt{strategy-choice}).
This privilege is used to specify the set of authorized capabilities for a given NDN identity certificate in the configuration file.

\subsubsection{Internal Face and Internal Client Face}\label{sec:mgmt-intface}

To initialize the \texttt{Dispatcher}, a reference to a \texttt{Internal Client Face} derived from the \texttt{ndn::Face} class, is provided, which is connected to a \texttt{Internal Face} derived from the \texttt{nfd::Face} for internal use in NFD.
Consequently, the dispatcher is granted to perform Interest / Data exchange bidirectionally with the \texttt{Forwarder}.

\subsubsection{Command Authenticator}\label{sec:mgmt-authenticator}

The \texttt{CommandAuthenticator} provides authentication and authorization of control commands.

Rules for authentication and authorization is specified in ``authorizations' section of the NFD configuration file.
In that section, each ``authorize'' subsection gives a single NDN certificate and a set of privileges.
An example of ``authorize'' subsection is:

\begin{verbatim}
authorize
{
  certfile keys/operator.ndncert
  privileges
  {
    faces
    strategy-choice
  }
}
\end{verbatim}

Unlike most other validators, this authenticator does not attempt to retrieve signing certificates during the authentication step.
This is because the authenticator needs to operate before routes are established.
Thus, every certificate used to sign control commands must be explicitly specified in the configuration.
The value in ``certfile'' key is a filename that refers to a NDN certification (version 1); it can be either an absolute path, or a path relative to the configuration file.
For convenience in non-production systems, a special value ``any'' can be used in ``certfile'' key, which would grant any signing certificate the priviledges listed in ``privileges'' subsection.
Under the ``privileges'' subsection, a set of privileges can be specified, where each privilege is the management module name.
Supported privileges are: \texttt{faces}, \texttt{fib}, \texttt{strategy-choice}.

The NFD initialization procedure creates a \texttt{CommandAuthenticator} instance, and invokes \texttt{CommandAuthenticator::setConfigFile} method in which the authenticator registers itself as a processor of ``authorizations'' section in NFD configuration file.
The authenticator instance is then given to the constructor of each manager that accepts control commands.
The manager invokes \texttt{CommandAuthenticator::makeAuthorization} method for each accepted command verb, which returns an \texttt{ndn::mgmt::Authorization} callback function that can be used with the dispatcher; this \texttt{makeAuthorization} invocation also tells the authenticator which management module names are supported.
Finally, the initial configuration is parsed, which populates the internal data structures of the authenticator.

When a control command arrives, the dispatcher passes the command Interest to the \texttt{ndn::mgmt::Authorization} callback function returned by \texttt{CommandAuthenticator::makeAuthorization}.
This authorization callback has four steps:

\begin{compactenum}
\item Based on management module and command verb, find out what signing certificates are allowed.
      Although the dispatcher does not directly pass management module and command verb to the authorization callback, this information is captured when the authorization callback was initially constructed, because the manager invokes \texttt{CommandAuthenticator::makeAuthorization} once for every distinct command verb and thus each gets a different authorization callback instance.
\item If ``certfile any'' is applicable to this management module and command verb, the control command is authorized, and the remaining steps are skipped.
\item Check whether the Interest is signed by one of these certificates by inspecting the KeyLocator field.
      If not, the control command is rejected.
\item Pass the Interest to the \texttt{ndn::security::CommandInterestValidator} which checks timestamps on command Interests to prevent replay attacks.
      A single command Interest validator instance is shared among all authorization callbacks.
      If the timestamp is unacceptable, the control command is rejected.
\item Finally, the signature is verified against the public key using \texttt{Validator::verifySignature} function, and the control command is either accepted or rejected based on the result from signature verification.
\end{compactenum}

The benefit of this design is that it takes advantage of the management module and command verb information that is already known from the dispatcher, so that name lookups are not repeated to determine permissible signing certificates.

A weakness in this procedure is a risk of denial-of-service attack: signature verification is performed after timestamp checking; an attacker can forge command Interests with the KeyLocator of a permissible signing certificate to manipulate the internal state of the command Interest validator, and cause subsequent control commands signed by that certificate to be rejected. \url{https://redmine.named-data.net/issues/3821}

\subsection{Forwarder Status}\label{sec:mgmt-status}

The Forwarder Status Manager (\texttt{nfd::StatusServer}) provides information about the NFD and basic statistics about NFD by the method \texttt{listStatus}, which is
registered to the dispatcher with the name \texttt{status} (no verb).
The supplied information includes the NFD's version, startup time, Interest/Data packet counts, and various table entry counts, and is published with a 5 second freshness time to avoid excessive processing.


\subsection{Face Management}\label{sec:mgmt-face}

The Face Manager (\texttt{nfd::FaceManager}) creates and destroys faces for its configured protocol types.
Local fields can also be enabled/disabled to learn over which face an Interest or Data packet arrived, to set the caching policy of a Data packet, and to direct an Interest out a specific face.

\paragraph{\textbf{Command Processing}}\

On creation, the Face Manger registers four command handlers, \texttt{createFace}, \texttt{destroyFace}, \texttt{enableLocalControl}, \texttt{disableLocalControl}, to the dispatcher with names \path{faces/create}, \path{faces/destroy}, \path{faces/enable-local-control} and \path{faces/disable-local-control} respectively.

\begin{compactitem}
\item \texttt{createFace}: create unicast TCP/UDP Faces
\item \texttt{destroyFace}: destroy Faces
\item \texttt{enableLocalControl}: enable local fields on the requesting face
\item \texttt{disableLocalControl}: disable local fields on the requesting face
\end{compactitem}

While NFD supports a range of different protocols, the Face management protocol currently only supports the creation of unicast TCP and UDP Faces during runtime.
That said, the Face Manager may also be configured to have other channel types to listen for incoming connections and create Faces on demand.

\vspace{0.25cm}
\texttt{createFace} uses \texttt{FaceUri} to parse the incoming URI in order to determine the type of Face to make.
The URI must be canonical.
A canonical URI for UDP and TCP tunnels should specify either IPv4 or IPv6, have IP address instead of hostname, and contain port number (e.g., ``\texttt{udp4://192.0.2.1:6363}'' is canonical, but ``\texttt{udp://192.0.2.1}'' and ``\texttt{udp://example.net:6363}'' are not).
Non-canonical URI results in a code 400 ``Non-canonical URI'' control response.
The URI's scheme (e.g., ``\texttt{tcp4}'', ``\texttt{tcp6}'', etc.) is used to lookup the appropriate \texttt{ProtocolFactory} via \texttt{FaceSystem::getFactoryByScheme}.
Failure to find a factory results in a code 501 ``unsupported protocol'' control response.
Otherwise, Face Managers calls \texttt{ProtocolFactory::\allowbreak createFace} method to initiate asynchronous process of face creation (DNS resolution, connection to remote host, etc.), supplying \texttt{afterCreateFaceSuccess} and \texttt{afterCreateFaceFailure} callbacks.
These callbacks will be called by the face system after the Face is either successfully created or failed to be created, respectively.

After Face has been created (from \texttt{afterCreateFaceSuccess} callback), the Face Manager adds the new Face to the Face Table \footnote{The Face Table is a table of Faces that is managed by the Forwarder.  Using this table, the Forwarder assigns each Face a unique ID, manage active Faces, and perform lookup for a Face object by ID when requested by other modules.} and responds with a code 200 ``success'' control response to the original control command.
Unauthorized, improperly-formatted requests and requests when Face is failed to be created will be responded with appropriate failure codes and failure reasons.
Refer to the NFD Management protocol specification~\cite{nfd-management} for the list of possible error codes.

\vspace{0.25cm}
\texttt{destroyFace} attempts to close the specified Face.
The Face Manager responds with code 200 ``Success'' if the Face is successfully destroyed or it cannot be found in the Face Table, but no errors occurred.
The Face Manager does not directly remove the Face from the Face Table, but it is a side effect of calling \texttt{Face::close}.

\vspace{0.25cm}
LocalControlHeader can be enabled on local Faces (\texttt{UnixStreamFace} and \texttt{TcpLocalFace}) in order to expose some internal NFD state to the application or to give the application some control over packet processing.
Currently LocalControlHeader specification defines the following \emph{local control features}:

\begin{compactitem}
\item \texttt{IncomingFaceId}: provide the \texttt{FaceId} that Data packets arrive from
\item \texttt{NextHopFaceId}: forward Interests out the Face with a given \texttt{FaceId}
\end{compactitem}

As their names imply, the \texttt{(enable|disable)LocalControl} methods enable and disable the specified local control features on the Face sending the control command.
Both methods utilize \texttt{extractLocalControlParameters} method to perform common functionality of option validation and ensuring that the requesting Face is local.
When incorrectly formatted, unauthorized request or request from a non-local Face is received, the Face Manager responds with an appropriate error code.
Command success, as defined by Control Command specification~\cite{control-command}, is always responded with code 200~``OK'' response.

\paragraph{\textbf{Datasets and Event Notification}}\

The Face Manager provides two datasets: Channel Status and Face Status.
The Channel Status dataset lists all channels (in the form of their local URI) that this NFD has created and can be accessed under the \path{/localhost/nfd/faces/channels} namespace.
Face Status, similarly, lists all created Faces, but provides much more detailed information, such as flags and incoming/outgoing Interest/Data counts.
The Face Status dataset can be retrieved from the \path{/localhost/nfd/faces/list} namespace.

These datasets are supplied when \texttt{listFaces} and \texttt{listChannels} methods are invoked.
Besides, another method \texttt{queryFaces} is provided to supply the status of face with a specified name.
When the Face Manger is constructed, it will register these three handlers to the dispatcher with names \path{faces/list}, \path{faces/channels} and \path{faces/query} respectively.

In addition to these datasets, the Face Manager also publishes notifications when Faces are created and destroyed.
This is done using the \texttt{postNotification} function returns after registering a notification stream to the dispatcher with the name \path{/faces/events}.
Two methods, \texttt{afterFaceAdded} and \texttt{afterFaceRemoved}, that take the function \texttt{postNotification} as a argument, are set as connections to the
Face Table's \texttt{onAdd} and \texttt{onRemove} Signals~\cite{ndn-cxx-guide}.
Once these two signals are emitted, the connected methods will be invoked immediately, where the \texttt{postNotification} will be used to publish notifications through the dispatcher.


\subsection{FIB Management}\label{sec:mgmt-fib}

The FIB Manager (\texttt{nfd::FibManager}) allows authorized users (normally, it is only RIB Manager daemon, see Section~\ref{sec:rib}) to modify NFD's FIB and publishes a dataset of all FIB entries and their next hops.
At a high-level, authorized users can request the FIB Manager to:

\begin{enumerate} \itemsep -2pt
\item add a next hop to a prefix
\item update the routing cost of reaching a next hop
\item remove a next hop from a prefix
\end{enumerate}

The first two capabilities correspond to the \texttt{add-nexthop} verb, while removing a next hop falls under \texttt{remove-nexthop}.
These two verbs are used along with the manager name \texttt{fib} to register the following handlers of control commands:

\begin{compactitem}
\item \texttt{addNextHop}: add next hop or update existing hop's cost
\item \texttt{removeNextHop}: remove specified next hop
\end{compactitem}

Note that \texttt{addNextHop} will create a new FIB entry if the requested entry does not already exist.
Similarly, \texttt{removeNextHop} will remove the FIB entry after removing the last next hop.

\paragraph{\textbf{FIB Dataset}}\
One status dataset handler, \texttt{listEntries} is registered, when the FIB Manger is constructed, to the dispatcher with the name \path{fib/list}, to publish FIB entries according to the FIB dataset specification.
On invocation, the whole FIB is serialized in the form of a collection of \texttt{FibEntry} and nested \texttt{NextHopList} TLVs, which are appended to a \emph{StatusDatasetContext} of the dispatcher.
After all parts are appended, that context is ended and will process all received status data.


\subsection{Strategy Choice Management}\label{sec:mgmt-sc}

The Strategy Choice Manager (\texttt{nfd::StrategyChoiceManager}) is responsible for setting and unsetting forwarding strategies for the namespaces via the Strategy Choice table.
Note that setting/unsetting the strategy applies only to the local NFD.
Also, the current implementation requires that the selected strategy must have been added to a pool of known strategies in NFD at compile time (see
Section~\ref{sec:strategy}.
Attempting to change to an unknown strategy will result in a code 504 ``unsupported strategy'' response.
By default, there is at least the root prefix (``/'') available for strategy changes, which defaults to the ``best route'' strategy.
However, it is an error to attempt to unset the strategy for root (code 403).

Similar to the FIB and Face Managers, the Strategy Choice Manager registers, when constructed, two command handlers, \texttt{setStrategy} and \texttt{unsetStrategy}, as well as a status dataset handler \texttt{listChoices} to the dispatcher, with names \path{strategy-choice/set}, \path{strategy-choice/unset}, and \path{strategy-choice/list} respectively.

On invocation, \texttt{setStrategy} and \texttt{unsetStrategy} will set / unset the specified strategy, while \texttt{listChoices} will serialize the Strategy Choice table into \texttt{StrategyChoice} TLVs, and publish them as the dataset.


\subsection{Configuration Handlers}\label{sec:mgmt-conf}

\subsubsection{General Configuration File Section Parser}\label{sec:mgmt-conf-general}

The \texttt{general} namespace provides parsing for the identically named \texttt{general} configration file section.
The NFD startup process invokes \texttt{setConfigSection} to trigger the corresponding localized (static) \texttt{onConfig} method for parsing.

At present, this section is limited to specifying an optional user and group name to drop the effective \texttt{userid} and \texttt{groupid} for safer operation.
The \texttt{general} section parser initializes a global \texttt{PrivilegeHelper} instance to perform the actual (de-)escalation work.


\subsubsection{Tables Configuration File Section Parser}\label{sec:mgmt-conf-tables}

\texttt{TablesConfigSection} provides parsing for the \texttt{tables} configuration file section.
This class can then configure the various NFD tables (CS, PIT, FIB, Strategy Choice, Measurements, and Network Region) appropriately.
Currently, the \texttt{tables} section supports changing Content Store capacity and cache replacement policy, per-prefix strategy choices, and network region names.
Like other configuration file parsers, \texttt{TablesConfigSection} is registered as the processor of its corresponding section by the NFD startup process via \texttt{setConfigFile} method, which invokes \texttt{onConfig}.


\subsection{How to Extend NFD Management}\label{sec:mgmt-extend}

Each manager is an interface for some part of the lower layers of NFD.
For example, the Face Manager handles Face creation/destruction.
The current set of managers are independent and do not interact with one another.
Consequently, adding a new manager is a fairly straightforward task; one only needs to determine what part(s) of NFD should be exported to an Interest/Data API and create an appropriate command Interest interpreter.

In general, NFD managers do not need to offer much functionality through a programatic API.
Most managers only need to register request handlers or notification streams to the \emph{ndn::Dispatcher}, such that the corresponding requests will be routed to them and the produced notifications could be post out.
Some managers may also require to hook into the configuration file parser.
All managerial tasks to control NFD internals should be performed via the defined Interest/Data management protocol.
